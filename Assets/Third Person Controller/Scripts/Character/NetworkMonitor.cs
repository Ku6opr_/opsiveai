﻿#if !(UNITY_4_6 || UNITY_4_7 || UNITY_5_0)
using UnityEngine;
using UnityEngine.Networking;

namespace Opsive.ThirdPersonController
{
    /// <summary>
    /// The NetworkMonitor acts as an intermediary component between the network and any object related to the character that is not spawned. These objects do not have
    /// the NetworkIdentifier component so they cannot issue standard RPC or Command calls. It also performs various other network functions such as contain the NetworkMessage identifiers.
    /// </summary>
    public class NetworkMonitor : Photon.MonoBehaviour
    {
        [Tooltip("The distance to look ahead when aiming or determining an IK look position. Set to -1 to use a raycast from the character position.")]
        [SerializeField] protected float m_LookDistance = 10;

        // Internal variables
        private SharedMethod<int, GameObject> m_GameObjectWithItemID = null;
        private SharedProperty<Ray> m_TargetLookRay = null;
        private SharedProperty<Transform> m_TargetLock = null;
        private SharedProperty<float> m_Recoil = null;
        private SharedProperty<CameraMonitor.CameraViewMode> m_ViewMode = null;

        private Ray m_CameraTargetLookRay = new Ray();
        private Transform m_CameraTargetLock;
        private float m_CameraRecoil;
        private float m_SendInterval;
        private float m_LastSyncTime = -1;

        /// <summary>
        /// Cache the component references and initialize the default values.
        /// </summary>
        private void Awake()
        {
            SharedManager.Register(this);
        }

        /// <summary>
        /// Initializes all of the SharedFields.
        /// </summary>
        private void Start()
        {
            var camera = Utility.FindCamera(gameObject);
            SharedManager.InitializeSharedFields(camera.gameObject, this);
            SharedManager.InitializeSharedFields(gameObject, this);

            // The NetworkMonitor only needs to update for the camera. There is no camera for a non-local player so disable the component if not local.
            if (photonView.isMine) {
                camera.GetComponent<CameraController>().Character = gameObject;
            } else {
                enabled = false;
            }
        }

        /// <summary>
        /// Update the look variables of the camera. This is sent to all of the other clients so the clients can accurately update the character's IK.
        /// </summary>
        private void Update()
        {
            var lookRay = m_TargetLookRay.Get();
            // The server only needs to know the look position when the character is aiming. Don't sync every frame because that would cause too much bandwidth.
            if (m_LastSyncTime + 1.0f / PhotonNetwork.sendRate < Time.time) {
                CmdUpdateCameraVariables(lookRay.origin, lookRay.direction, m_Recoil.Get());
                var targetLock = m_TargetLock.Get();
                if (m_CameraTargetLock != targetLock) {
                    CmdUpdateCameraTargetLock(targetLock != null ? targetLock.gameObject : null);
                    m_CameraTargetLock = targetLock;
                }
                m_LastSyncTime = Time.time;
            }
            // Update the camera variables immediately on the local machine. There is no reason to wait for the server to update these variables.
            UpdateCameraVariables(lookRay.origin, lookRay.direction, m_Recoil.Get());
        }

        /// <summary>
        /// Tell the server the camera variables of the local player.
        /// </summary>
        /// <param name="origin">The origin of the camera's look ray.</param>
        /// <param name="direction">The direction of hte camera's look ray.</param>
        /// <param name="recoil">Any recoil that should be added.</param>
        [PunRPC]
        public void CmdUpdateCameraVariables(Vector3 origin, Vector3 direction, float recoil)
        {
            photonView.RPC("RpcUpdateCameraVariables", PhotonTargets.Others, origin, direction, recoil);
        }

        /// <summary>
        /// Send all of the camera variables to the clients.
        /// </summary>
        /// <param name="origin">The origin of the camera's look ray.</param>
        /// <param name="direction">The direction of hte camera's look ray.</param>
        /// <param name="recoil">Any recoil that should be added.</param>
        [PunRPC]
        public void RpcUpdateCameraVariables(Vector3 origin, Vector3 direction, float recoil)
        {
            if (!photonView.isMine) {
                UpdateCameraVariables(origin, direction, recoil);
            }
        }

        /// <summary>
        /// Tells the server the camera lock value.
        /// </summary>
        /// <param name="targetLock">The value of the camera lock.</param>
        [PunRPC]
        private void CmdUpdateCameraTargetLock(GameObject targetLock)
        {
            photonView.RPC("RpcUpdateCameraTargetLock", PhotonTargets.Others, targetLock);
        }

        /// <summary>
        /// Send the camera lock value to the clients.
        /// </summary>
        /// <param name="origin">The value of the camera lock.</param>
        [PunRPC]
        private void RpcUpdateCameraTargetLock(GameObject targetLock)
        {
            if (!photonView.isMine) {
                m_CameraTargetLock = targetLock.transform;
            }
        }

        /// <summary>
        /// Update the camera variables for the attached character.
        /// </summary>
        /// <param name="origin">The origin of the camera's look ray.</param>
        /// <param name="direction">The direction of hte camera's look ray.</param>
        /// <param name="recoil">Any recoil that should be added.</param>
        private void UpdateCameraVariables(Vector3 origin, Vector3 direction, float recoil)
        {
            m_CameraTargetLookRay.origin = origin;
            m_CameraTargetLookRay.direction = direction;
            m_CameraRecoil = recoil;
        }

        /// <summary>
        /// Returns the direction that the camera is looking. An example of where this is used include when the GUI needs to determine if the crosshairs is looking at any enemies.
        /// </summary>
        /// <param name="applyRecoil">Should the target ray take into account any recoil?</param>
        /// <returns>A ray in the direction that the camera is looking.</returns>
        private Vector3 SharedMethod_TargetLookDirection(bool applyRecoil)
        {
            return CameraMonitor.TargetLookDirection(m_CameraTargetLookRay, m_CameraTargetLock, applyRecoil ? m_CameraRecoil : 0);
        }

        /// <summary>
        /// Returns the direction that the camera is looking.
        /// </summary>
        /// <param name="lookPoint">The reference point to compute the direction from.</param>
        /// <param name="raycastLookDistance">Should the raycast look distance be used?</param>
        /// <returns>The direction that the camera is looking.</returns>
        public Vector3 SharedMethod_TargetLookDirectionLookPoint(Vector3 lookPoint, bool raycastLookDistance)
        {
            // The SharedMethod may be called before Start is called.
            if (m_ViewMode == null) {
                SharedManager.InitializeSharedFields(Utility.FindCamera(gameObject).gameObject, this);
            }

            return CameraMonitor.TargetLookDirection(m_CameraTargetLookRay, lookPoint, m_CameraTargetLock, m_CameraRecoil, m_LookDistance,  m_ViewMode.Get());
        }

        /// <summary>
        /// Returns the point that the canera is looking at.
        /// </summary>
        /// <returns>The point that the camera is looking at.</returns>
        public Vector3 SharedMethod_TargetLookPosition()
        {
            // The SharedMethod may be called before Start is called.
            if (m_ViewMode == null) {
                SharedManager.InitializeSharedFields(Utility.FindCamera(gameObject).gameObject, this);
            }

            return CameraMonitor.TargetLookPosition(m_CameraTargetLookRay, m_CameraTargetLock, m_LookDistance, m_ViewMode.Get());
        }

        /// <summary>
        /// Execute an event on all of the clients. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        public void ExecuteItemEvent(int itemID, string eventName)
        {
            photonView.RPC("RpcExecuteItemEvent", PhotonTargets.AllViaServer, itemID, eventName);
        }

        /// <summary>
        /// Execute an event on the client. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        [PunRPC]
        public void RpcExecuteItemEvent(int itemID, string eventName)
        {
            EventHandler.ExecuteEvent(m_GameObjectWithItemID.Invoke(itemID), eventName);
        }

        /// <summary>
        /// Execute an event on all of the clients with one argument. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        public void ExecuteItemEvent(int itemID, string eventName, Vector3 arg1)
        {
            photonView.RPC("RpcExecuteItemEventVector3", PhotonTargets.AllViaServer, itemID, eventName, arg1);
        }

        /// <summary>
        /// Execute an event on the client with one argument. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// Note: A new method name was used because of a current Unity bug (697809).
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        /// <param name="arg1">The first argument.</param>
        [PunRPC]
        public void RpcExecuteItemEventVector3(int itemID, string eventName, Vector3 arg1)
        {
            EventHandler.ExecuteEvent(m_GameObjectWithItemID.Invoke(itemID), eventName, arg1);
        }

        /// <summary>
        /// Execute an event on all of the clients with two arguments. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        public void ExecuteItemEvent(int itemID, string eventName, Vector3 arg1, Vector3 arg2)
        {
            photonView.RPC("RpcExecuteItemEventTwoVector3", PhotonTargets.AllViaServer, itemID, eventName, arg1, arg2);
        }

        /// <summary>
        /// Execute an event on the client with two arguments. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// Note: A new method name was used because of a current Unity bug (697809).
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        [PunRPC]
        public void RpcExecuteItemEventTwoVector3(int itemID, string eventName, Vector3 arg1, Vector3 arg2)
        {
            EventHandler.ExecuteEvent(m_GameObjectWithItemID.Invoke(itemID), eventName, arg1, arg2);
        }

        /// <summary>
        /// Execute an event on all of the clients with three arguments. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <param name="arg3">The third argument.</param>
        public void ExecuteItemEvent(int itemID, string eventName, GameObject arg1, Vector3 arg2, Vector3 arg3)
        {
#if UNITY_EDITOR
            if (arg1.GetComponent<PhotonView>() == null) {
                Debug.LogError("Error: " + arg1 + " " + arg1.name + " must have the PhotonView component added to it.");
            }
#endif
            PhotonView arg1PhotonView = arg1.GetPhotonView();

            if (arg1PhotonView == null)
            {
                Debug.LogError(arg1.name + " should have PhotonView attached to be passed over network");
            }

            photonView.RPC("RpcExecuteItemEventGameObjectTwoVector3", PhotonTargets.AllViaServer, itemID, eventName, arg1.GetPhotonView().viewID, arg2, arg3);
        }

        /// <summary>
        /// Execute an event on the client with three arguments. Items will call this method because the items do not have a NetworkIdentifier and cannot call Rpc methods.
        /// Note: A new method name was used because of a current Unity bug (697809).
        /// </summary>
        /// <param name="itemID">The id of the item executing the event.</param>
        /// <param name="eventName">The name of the event to be executed.</param>
        /// <param name="arg1">The first argument.</param>
        /// <param name="arg2">The second argument.</param>
        /// <param name="arg3">The third argument.</param>
        [PunRPC]
        public void RpcExecuteItemEventGameObjectTwoVector3(int itemID, string eventName, int arg1ViewId, Vector3 arg2, Vector3 arg3)
        {
            GameObject arg1 = PhotonView.Find(arg1ViewId).gameObject;

            EventHandler.ExecuteEvent<Transform, Vector3, Vector3>(m_GameObjectWithItemID.Invoke(itemID), eventName, arg1.transform, arg2, arg3);
        }
    }
}
#endif