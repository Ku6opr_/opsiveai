﻿using Opsive.ThirdPersonController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonOffline : MonoBehaviour
{
	void Awake ()
    {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.CreateRoom("offline");

        EventHandler.ExecuteEvent("OnStartClient");
    }
}
