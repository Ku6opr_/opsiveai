using UnityEngine;
#if ENABLE_MULTIPLAYER
using UnityEngine.Networking;
#endif
using System;
using System.Collections.Generic;

namespace Opsive.ThirdPersonController
{
    /// <summary>
    /// It is relatively expensive to instantiate new objects so reuse the objects when possible by placing them in a pool.
    /// </summary>
#if ENABLE_MULTIPLAYER
    public class ObjectPool : Photon.MonoBehaviour, IPunPrefabPool
#else
    public class ObjectPool : MonoBehaviour
#endif
    {
#if ENABLE_MULTIPLAYER
        [Tooltip("A list of prefabs that should be pooled on the client. These prefabs should not be listed in the NetworkManager spawnable prefabs list as well")]
        [SerializeField] protected List<GameObject> m_SpawnablePrefabs;
#endif

        // Static variables
        private static ObjectPool s_Instance;
        private static ObjectPool Instance
        {
            get
            {
#if UNITY_EDITOR
                if (!m_Initialized) {
                    Debug.LogWarning("Warning: ObjectPool is null. A GameObject has been created with the component automatically added. Please run Scene Setup from the Start Window.");
                    s_Instance = new GameObject("ObjectPool").AddComponent<ObjectPool>();
                }
#endif
                return s_Instance;
            }
        }

        // Internal variables
#if UNITY_EDITOR
        private static bool m_Initialized;
#endif
        private Dictionary<string, Stack<GameObject>> m_GameObjectPool = new Dictionary<string, Stack<GameObject>>();
        //private Dictionary<string, string> m_InstantiatedGameObjects = new Dictionary<string, string>();
        private Dictionary<Type, object> m_GenericPool = new Dictionary<Type, object>();
#if ENABLE_MULTIPLAYER
        private static Dictionary<string, GameObject> m_AssetIDPrefabMap = new Dictionary<string, GameObject>();
        private static HashSet<GameObject> m_NetworkSpawnedGameObjects = new HashSet<GameObject>();
#endif

        /// <summary>
        /// Assign the static variables and register for any events that the pool should be aware of.
        /// </summary>
        private void OnEnable()
        {
            s_Instance = this;
#if ENABLE_MULTIPLAYER
            PhotonNetwork.PrefabPool = this;
#endif
#if UNITY_EDITOR
            m_Initialized = true;
#endif
        }

#if ENABLE_MULTIPLAYER
        /// <summary>
        /// Registers all of the spawnable prefabs to the ClientScene handler. This will allow the objects to be pooled on the client in an network game.
        /// </summary>
        private void Awake()
        {
             m_AssetIDPrefabMap.Clear();
            for (int i = 0; i < m_SpawnablePrefabs.Count; ++i) {
                var networkID = m_SpawnablePrefabs[i].name;
                if (networkID != null) {
                    m_AssetIDPrefabMap.Add(networkID, m_SpawnablePrefabs[i]);
//#if UNITY_EDITOR
//                    // Quick sanity check within the editor only.
//                    var registeredSpawnablePrefabs = NetworkManager.singleton.spawnPrefabs;
//                    for (int j = 0; j < registeredSpawnablePrefabs.Count; ++j) {
//                        if (registeredSpawnablePrefabs[j].Equals(m_SpawnablePrefabs[i])) {
//                            Debug.LogWarning("Warning: " + m_SpawnablePrefabs[i] + " is added to both the ObjectPool and NetworkManager spawnable prefabs. Ensure it is " +
//                                             "only added in one location.");
//                            continue;
//                        }
//                    }
//#endif
                }
            }
        }

        /// <summary>
        /// This is called when PUN wants to create a new instance of an entity prefab. Must return valid GameObject with PhotonView.        /// 
        /// </summary>
        /// <param name="prefabId">The id of this prefab.</param>
        /// <param name="position">The position we want the instance instantiated at.</param>
        /// <param name="rotation">The rotation we want the instance to take.</param>
        /// <returns>The newly instantiated object, or null if a prefab with <paramref name="prefabId"/> was not found.</returns>
        public GameObject Instantiate(string prefabId, Vector3 position, Quaternion rotation)
        {
            GameObject prefab;
            if (m_AssetIDPrefabMap.TryGetValue(prefabId, out prefab))
            {
                return Instantiate(prefab, position, rotation);
            }

            Debug.LogError(prefabId + " should be added to ObjectPool SpawnablePrefabs list");

            return null;
        }
#endif

        /// <summary>
        /// Instantiate a new GameObject. Use the object pool if a previously used GameObject is located in the pool, otherwise instaniate a new GameObject.
        /// </summary>
        /// <param name="original">The original GameObject to pooled a copy of.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <returns>The pooled/instantiated GameObject.</returns>
        public static GameObject Instantiate(GameObject original, Vector3 position, Quaternion rotation)
        {
            return Instantiate(original, position, rotation, null);
        }

        /// <summary>
        /// Spawn a new GameObject on the server and persist to the clients. Use the object pool if a previously used GameObject is located in the pool, otherwise instaniate a new GameObject.
        /// </summary>
        /// <param name="original">The original GameObject to pooled a copy of.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <returns>The pooled/instantiated GameObject.</returns>
        public static GameObject Spawn(GameObject original, Vector3 position, Quaternion rotation)
        {
            return Spawn(original, position, rotation, null);
        }

        /// <summary>
        /// Spawn a new GameObject on the server and persist to the clients. Use the object pool if a previously used GameObject is located in the pool, otherwise instaniate a new GameObject.
        /// </summary>
        /// <param name="original">The original GameObject to pooled a copy of.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <param name="parent">The parent to assign to the pooled GameObject.</param>
        /// <returns>The pooled/instantiated GameObject.</returns>
        public static GameObject Spawn(GameObject original, Vector3 position, Quaternion rotation, Transform parent)
        {
            return Instance.InstantiateInternal(original, position, rotation, parent, true);
        }

        /// <summary>
        /// Instantiate a new GameObject. Use the object pool if a previously used GameObject is located in the pool, otherwise instaniate a new GameObject.
        /// </summary>
        /// <param name="original">The original GameObject to pooled a copy of.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <param name="parent">The parent to assign to the pooled GameObject.</param>
        /// <returns>The pooled/instantiated GameObject.</returns>
        public static GameObject Instantiate(GameObject original, Vector3 position, Quaternion rotation, Transform parent)
        {
            return Instance.InstantiateInternal(original, position, rotation, parent, false);
        }

       

        /// <summary>
        /// Internal method to instantiate a new GameObject. Use the object pool if a previously used GameObject is located in the pool, otherwise instaniate a new GameObject.
        /// </summary>
        /// <param name="original">The original GameObject to pooled a copy of.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <param name="parent">The parent to assign to the pooled GameObject.</param>
        /// <param name="networkSpawn">Should the object be spawned on the server and persisted across clients?</param>
        /// <returns>The pooled/instantiated GameObject.</returns>
        private GameObject InstantiateInternal(GameObject original, Vector3 position, Quaternion rotation, Transform parent, bool networkSpawn)
        {
            var originalInstanceID = original.name;
            var instantiatedObject = ObjectFromPool(originalInstanceID, position, rotation, parent, networkSpawn);
            if (instantiatedObject == null)
            {
                instantiatedObject = (GameObject)GameObject.Instantiate(original, position, rotation);
                instantiatedObject.name = originalInstanceID;
                instantiatedObject.transform.parent = parent;
                // Map the newly instantiated instance ID to the original instance ID so when the object is returned it knows what pool to go to.
                //Debug.Log("Add object to pool: " + instantiatedObject.name);
                //m_InstantiatedGameObjects.Add(instantiatedObject.name, originalInstanceID);
            }

#if ENABLE_MULTIPLAYER
            if (networkSpawn && PhotonNetwork.isMasterClient) {
                instantiatedObject.SetActive(false);
                //instantiatedObject.transform.parent = transform;

                Stack<GameObject> pool;
                if (m_GameObjectPool.TryGetValue(instantiatedObject.name, out pool))
                {
                    pool.Push(instantiatedObject);
                }
                else
                {
                    // The pool for this GameObject type doesn't exist yet so it has to be created.
                    pool = new Stack<GameObject>();
                    pool.Push(instantiatedObject);
                    m_GameObjectPool.Add(instantiatedObject.name, pool);
                }

                m_NetworkSpawnedGameObjects.Add(instantiatedObject);                
                PhotonNetwork.InstantiateSceneObject(original.name, position, rotation, 0, null);
            }
#endif

            return instantiatedObject;
        }

        /// <summary>
        /// An object is trying to be popped from the object pool. Return the pooled object if it exists otherwise null meaning one needs to be insantiated.
        /// </summary>
        /// <param name="originalInstanceID">The instance id of the GameObject trying to be popped from the pool.</param>
        /// <param name="position">The position of the pooled GameObject.</param>
        /// <param name="rotation">The rotation of the pooled Gameobject.</param>
        /// <param name="parent">The parent to assign to the pooled GameObject.</param>
        /// <param name="networkSpawn">Should the object be spawned on the server and persisted across clients?</param>
        /// <returns>The pooled GameObject.</returns>
        private GameObject ObjectFromPool(string originalInstanceID, Vector3 position, Quaternion rotation, Transform parent, bool networkSpawn)
        {
            
            Stack<GameObject> pool;
            if (m_GameObjectPool.TryGetValue(originalInstanceID, out pool)) {
                if (pool.Count > 0) {
                    var instantiatedObject = pool.Pop();
                    instantiatedObject.transform.position = position;
                    instantiatedObject.transform.rotation = rotation;
                    instantiatedObject.transform.parent = parent;
                    instantiatedObject.SetActive(true);
                    // Map the newly instantiated instance ID to the original instance ID so when the object is returned it knows what pool to go to.
                    //m_InstantiatedGameObjects.Add(instantiatedObject.name, originalInstanceID);
                    //Debug.Log(originalInstanceID + " in pool");
                    return instantiatedObject;
                }
            }
            //Debug.Log("no " + originalInstanceID + " in pool");
            return null;
        }

        /// <summary>
        /// Return if the object was instantiated with the ObjectPool.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to check to see if it was instantiated with the ObjectPool.</param>
        /// <returns>True if the object was instantiated with the ObjectPool.</returns>
        public static bool SpawnedWithPool(GameObject instantiatedObject)
        {
            return Instance.SpawnedWithPoolInternal(instantiatedObject);
        }

        /// <summary>
        /// Internal method to return if the object was instantiated with the ObjectPool.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to check to see if it was instantiated with the ObjectPool.</param>
        /// <returns>True if the object was instantiated with the ObjectPool.</returns>
        private bool SpawnedWithPoolInternal(GameObject instantiatedObject)
        {
            return m_GameObjectPool.ContainsKey(instantiatedObject.name);
            //return m_InstantiatedGameObjects.ContainsKey(instantiatedObject.name);
        }

        /// <summary>
        /// Return the instance ID of the prefab used to spawn the instantiated object.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to get the original instance ID</param>
        /// <returns>The original instance ID</returns>
        public static string OriginalInstanceID(GameObject instantiatedObject)
        {
            return Instance.OriginalInstanceIDInternal(instantiatedObject);
        }

        /// <summary>
        /// Internal method to return the instance ID of the prefab used to spawn the instantiated object.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to get the original instance ID</param>
        /// <returns>The original instance ID</returns>
        private string OriginalInstanceIDInternal(GameObject instantiatedObject)
        {
            return instantiatedObject.name;
            /*var instantiatedInstanceID = instantiatedObject.name;
            string originalInstanceID = null;
            if (!m_InstantiatedGameObjects.TryGetValue(instantiatedInstanceID, out originalInstanceID)) {
                Debug.LogError("Unable to get the original instance ID of " + instantiatedObject + ": has the object already been placed in the ObjectPool?");
                return null;
            }
            return originalInstanceID;*/
        }

        /// <summary>
        /// Photon: This is called when PUN wants to destroy the instance of an entity prefab.
        /// Return the specified GameObject back to the ObjectPool.
        /// </summary>
        /// <remarks>
        /// A pool needs some way to find out which type of GameObject got returned via Destroy().
        /// It could be a tag or name or anything similar.
        /// </remarks>
        /// <param name="gameObject">
        /// Photon: The instance to destroy.
        /// The GameObject to return to the pool
        /// </param>
        public void Destroy(GameObject instantiatedObject)
        {
            DestroyInternal(instantiatedObject);
        }

        public static void DestroyStatic(GameObject instantiatedObject)
        {
            if (Instance == null)
            {
                return;
            }
            Instance.DestroyInternal(instantiatedObject);
        }

        /// <summary>
        /// Internal method to return the specified GameObject back to the ObjectPool. Call the corresponding server or client method.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to return to the pool.</param>
        private void DestroyInternal(GameObject instantiatedObject)
        {
            //var instantiatedInstanceID = instantiatedObject.name;
            //string originalInstanceID = null;
            //if (!m_InstantiatedGameObjects.TryGetValue(instantiatedInstanceID, out originalInstanceID)) { 
            //    Debug.LogError("Unable to pool " + instantiatedObject + " (instance " + instantiatedInstanceID + "): the GameObject was not instantiated with ObjectPool.Instantiate " + Time.time);
            //    return;
            //}

            // Map the instantiated instance ID back to the orignal instance ID so the GameObject can be returned to the correct pool.
            //m_InstantiatedGameObjects.Remove(instantiatedInstanceID);

            DestroyLocal(instantiatedObject);//, originalInstanceID);
        }

        /// <summary>
        /// Return the specified GameObject back to the ObjectPool.
        /// </summary>
        /// <param name="instantiatedObject">The GameObject to return to the pool.</param>
        /// <param name="originalInstanceID">The instance ID of the original GameObject.</param>
        private void DestroyLocal(GameObject instantiatedObject)//, string originalInstanceID)
        {
            // This GameObject may have a collider and that collider may be ignoring the collision with other colliders. Revert this setting because the object is going
            // back into the pool.
            Collider instantiatedObjectCollider;
            if ((instantiatedObjectCollider = Utility.GetComponentForType<Collider>(instantiatedObject)) != null) {
                LayerManager.RevertCollision(instantiatedObjectCollider);
            }
            instantiatedObject.SetActive(false);
            instantiatedObject.transform.parent = transform;

            Stack<GameObject> pool;
            if (m_GameObjectPool.TryGetValue(instantiatedObject.name, out pool)) {
                pool.Push(instantiatedObject);
            } else {
                // The pool for this GameObject type doesn't exist yet so it has to be created.
                pool = new Stack<GameObject>();
                pool.Push(instantiatedObject);
                m_GameObjectPool.Add(instantiatedObject.name, pool);
            }

#if ENABLE_MULTIPLAYER
            // NetworkServer.Unspawn will pool the object on the clients through the ClientUnspawnHandler.
            if (PhotonNetwork.isMasterClient && m_NetworkSpawnedGameObjects.Contains(instantiatedObject)) {
                m_NetworkSpawnedGameObjects.Remove(instantiatedObject);
                PhotonNetwork.Destroy(instantiatedObject);
            }
#endif
        }

        /// <summary>
        /// Get a pooled object of the specified type using a generic ObjectPool.
        /// </summary>
        /// <typeparam name="T">The type of object to get.</typeparam>
        /// <returns>A pooled object of type T.</returns>
        public static T Get<T>()
        {
            return Instance.GetInternal<T>();
        }

        /// <summary>
        /// Internal method to get a pooled object of the specified type using a generic ObjectPool.
        /// </summary>
        /// <typeparam name="T">The type of object to get.</typeparam>
        /// <returns>A pooled object of type T.</returns>
        private T GetInternal<T>()
        {
            object value;
            if (m_GenericPool.TryGetValue(typeof(T), out value)) {
                var pooledObjects = value as Stack<T>;
                if (pooledObjects.Count > 0) {
                    return pooledObjects.Pop();
                }
            }
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Return the object back to the generic object pool.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="obj">The object to return.</param>
        public static void Return<T>(T obj)
        {
            Instance.ReturnInternal<T>(obj);
        }

        /// <summary>
        /// Internal method to return the object back to the generic object pool.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="obj">The object to return.</param>
        private void ReturnInternal<T>(T obj)
        {
            object value;
            if (m_GenericPool.TryGetValue(typeof(T), out value)) {
                var pooledObjects = value as Stack<T>;
                pooledObjects.Push(obj);
            } else {
                var pooledObjects = new Stack<T>();
                pooledObjects.Push(obj);
                m_GenericPool.Add(typeof(T), pooledObjects);
            }
        }
    }
}