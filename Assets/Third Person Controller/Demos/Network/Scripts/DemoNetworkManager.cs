﻿using System;
using System.Linq;
using UnityEngine;
#if !(UNITY_4_6 || UNITY_4_7 || UNITY_5_0)
using UnityEngine.Networking;

namespace Opsive.ThirdPersonController.Demos.Networking
{
    /// <summary>
    /// Demo component which spawns the player in a unique location.
    /// </summary>
    public class DemoNetworkManager : Photon.PunBehaviour
    {
        #region Singleton 

        public static DemoNetworkManager Instance;

        private void InitSingleton()
        {
            if (Instance != null)
            {
                DestroyImmediate(this.gameObject);
                return;
            }

            Instance = this;

            DontDestroyOnLoad(this.gameObject);
        }

        #endregion

        [SerializeField]
        GameObject playerPrefab;

        private void Awake()
        {
            InitSingleton();

            PhotonNetwork.autoJoinLobby = false;

            PhotonNetwork.automaticallySyncScene = true;
        }

        private void Start()
        {
            if (playerPrefab == null)
            {
                Debug.Log("Player prefab name is not specified on DemoNetworkManager object");
                return;
            }

            Connect();
        }

        private void Connect()
        {
            PhotonNetwork.ConnectUsingSettings("1.0");
        }

        public override void OnConnectedToMaster()
        {
            PhotonNetwork.JoinRandomRoom();            
        }

        public override void OnJoinedRoom()
        {
            int playersCount = PhotonNetwork.playerList.Count();
            var spawnPosition = Vector3.right * playersCount;
            PhotonNetwork.Instantiate(playerPrefab.name, spawnPosition, Quaternion.identity, 0);
            
            if (PhotonNetwork.isMasterClient)
            {
                EventHandler.ExecuteEvent("OnNetworkAddFirstPlayer");
            }

            EventHandler.ExecuteEvent("OnStartClient");
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            EventHandler.ExecuteEvent("OnNetworkAddPlayer", newPlayer);
        }

        public override void OnLeftRoom()
        {
            EventHandler.ExecuteEvent("OnNetworkStopClient");
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            PhotonNetwork.CreateRoom(Guid.NewGuid().ToString(), new RoomOptions() { MaxPlayers = 16 }, null);
        }

        public override void OnCreatedRoom()
        {
            EventHandler.ExecuteEvent("OnNetworkServerReady");
        }

        /*/// <summary>
        /// Spawn the player in a unique location to prevent it overlapping with another character.
        /// </summary>
        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            var spawnPosition = Vector3.right * conn.connectionId;
            var player = GameObject.Instantiate(playerPrefab, spawnPosition, Quaternion.identity) as GameObject;
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);

#if UNITY_5_1 || UNITY_5_2
            // The new connection isn't active yet so check against 0 connections to determine if the server just started and objects can spawn.
            if (NetworkServer.connections.Count == 0) {
                EventHandler.ExecuteEvent("OnNetworkAddFirstPlayer");
            }
#else
            if (NetworkServer.connections.Count == 1) {
                EventHandler.ExecuteEvent("OnNetworkAddFirstPlayer");
            }
#endif
        }*/
    }
}
#endif