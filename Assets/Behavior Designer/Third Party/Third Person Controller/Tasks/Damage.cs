﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace BehaviorDesigner.Runtime.Tasks.ThirdPersonController
{
    [TaskDescription("Damage the Third Person Controller character. Returns Success if damaged.")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/documentation.php?id=111")]
    [TaskCategory("Third Person Controller")]
    [TaskIcon("Assets/Behavior Designer/Third Party/Third Person Controller/Editor/Icon.png")]
    public class Damage : Action
    {
        [Tooltip("A reference to the agent. If null it will be retrieved from the current GameObject.")]
        public SharedGameObject targetGameObject;
        [Tooltip("The amount of damage taken.")]
        public SharedFloat amount;
        [Tooltip("The position of the damage.")]
        public SharedVector3 position;
        [Tooltip("The amount of force applied to the object while taking the damage.")]
        public SharedVector3 force;
        [Tooltip("The radius of the explosive damage. If 0 then a non-exposive force will be used.")]
        public SharedFloat radius;

        private GameObject prevTarget;
        private Health health;

        public override TaskStatus OnUpdate()
        {
            var target = GetDefaultGameObject(targetGameObject.Value);
            if (target != prevTarget) {
                health = target.GetComponentInParent<Health>();
                prevTarget = target;
            }

            if (health == null) {
                return TaskStatus.Failure;
            }

            health.Damage(amount.Value, position.Value, force.Value, radius.Value);

            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            targetGameObject = null;
            amount = 0;
            position = Vector3.zero;
            force = Vector3.zero;
            radius = 0;
        }
    }
}