﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace BehaviorDesigner.Runtime.Tasks.ThirdPersonController
{
    [TaskDescription("Executes a Third Person Controller event.")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/documentation.php?id=111")]
    [TaskCategory("Third Person Controller")]
    [TaskIcon("Assets/Behavior Designer/Third Party/Third Person Controller/Editor/Icon.png")]
    public class ExecuteEvent : Action
    {
        [Tooltip("A reference to the agent to run the event on. If null it will be retrieved from the current GameObject.")]
        public SharedGameObject targetGameObject;
        [Tooltip("Is this the event global? If true then the target GameObject is not used")]
        public SharedBool globalEvent;
        [Tooltip("The name of the event")]
        public SharedString eventName;

        public override TaskStatus OnUpdate()
        {
            if (globalEvent.Value) {
                EventHandler.ExecuteEvent(eventName.Value);
            } else {
                var target = GetDefaultGameObject(targetGameObject.Value);
                EventHandler.ExecuteEvent(target, eventName.Value);
            }
            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            targetGameObject = null;
            globalEvent = false;
            eventName = "";
        }
    }
}