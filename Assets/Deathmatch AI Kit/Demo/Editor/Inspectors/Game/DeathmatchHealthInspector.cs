using UnityEngine;
using UnityEditor;
using Opsive.ThirdPersonController.Editor;
using Opsive.DeathmatchAIKit;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// Shows a custom inspector for DeathmatchHealth.
    /// </summary>
    [CustomEditor(typeof(DeathmatchHealth))]
    public class DeathmatchHealthInspector : CharacterHealthInspector
    {
        // Intentionally left blank. The parent class contains all of the implementation.
    }
}