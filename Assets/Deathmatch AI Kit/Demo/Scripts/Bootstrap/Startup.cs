﻿using UnityEngine;
#if !(UNITY_5_1 || UNITY_5_2)
using UnityEngine.SceneManagement;
#endif

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Prepares the scene for a deathmatch game by spawning the Deathmatch Manager prefab.
    /// </summary>
    public class Startup : MonoBehaviour
    {
        [Tooltip("The prefab to spawn upon startup")]
        [SerializeField] protected GameObject m_DeathmatchManagerPrefab;

#if UNITY_5_1 || UNITY_5_2
        // Internal variables
        private GameObject m_DeathmatchManagerObject;
#endif

        /// <summary>
        /// Spawn the DeathmatchManager prefab if there is no DeathmatchManager.
        /// </summary>
        private void Awake()
        {
            if (!DeathmatchManager.IsInstantiated) {
#if UNITY_5_1 || UNITY_5_2
                m_DeathmatchManagerObject = GameObject.Instantiate(m_DeathmatchManagerPrefab);
#else
                GameObject.Instantiate(m_DeathmatchManagerPrefab);
#endif
            } else {
                // If the Startup object is within a scene that already contains the DeathmatchManager then the scene was loaded from the main menu.
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// If a game scene is loaded then notify the DeathmatchManager that the scene was loaded.
        /// </summary>
        private void Start()
        {
#if UNITY_5_1 || UNITY_5_2
            // A non-0 value for loadedLevel indicates a game scene versus the main menu scene.
            if (Application.loadedLevel != 0) {
                m_DeathmatchManagerObject.GetComponent<DeathmatchManager>().SceneLoaded(Application.loadedLevel);
            }
#endif
            Destroy(gameObject);
        }
    }
}