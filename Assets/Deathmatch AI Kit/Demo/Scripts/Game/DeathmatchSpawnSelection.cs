﻿using UnityEngine;
using Opsive.ThirdPersonController;
using System.Collections.Generic;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Extends SpawnSelection to ensure there are no other enemies are nearby.
    /// </summary>
    public class DeathmatchSpawnSelection : SpawnSelection
    {
        // Static variables
        private static DeathmatchSpawnSelection s_Instance;
        private static DeathmatchSpawnSelection Instance { get { return s_Instance; } }

        [Tooltip("The first team inital spawn locations")]
        [SerializeField] protected Transform[] m_FirstTeamSpawnLocations;
        [Tooltip("The second team inital spawn locations")]
        [SerializeField] protected Transform[] m_SecondTeamSpawnLocations;
        [Tooltip("The third team inital spawn locations")]
        [SerializeField] protected Transform[] m_ThirdTeamSpawnLocations;
        [Tooltip("The fourth team inital spawn locations")]
        [SerializeField] protected Transform[] m_FourthTeamSpawnLocations;
        [Tooltip("The layer that the characters use.")]
        [SerializeField] protected LayerMask m_CharacterLayer = LayerManager.Enemy;
        [Tooltip("The radius of the sphere check which ensures there aren't any enemies nearby.")]
        [SerializeField] protected float m_Radius;

#if !(UNITY_5_1 || UNITY_5_2)
        // Internal variables
        private Collider[] m_HitColliders = new Collider[10];
#endif

        /// <summary>
        /// Assign the static variables.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            s_Instance = this;
        }

        /// <summary>
        /// Internal method for returning a random spawn location from the spawn location list.
        /// </summary>
        /// <returns>The Transform of a random spawn location.</returns>
        protected override Transform GetSpawnLocationInternal()
        {
            // Randomly choose a spawn location. If another character is within the radius of that spawn location then determine a new spawn location.
            var availableLocations = ObjectPool.Get<List<Transform>>();
            availableLocations.Clear();
            for (int i = 0; i < m_SpawnLocations.Length; ++i) {
                availableLocations.Add(m_SpawnLocations[i]);
            }
            // Randomly select a unique spawn location until there are no other characters near the spawn.
            for (int i = 0; i < m_SpawnLocations.Length; ++i) {
                var index = Random.Range(0, availableLocations.Count);
#if UNITY_5_1 || UNITY_5_2
                var overlapColliders = Physics.OverlapSphere(availableLocations[index].position, m_Radius, m_CharacterLayer.value);
                if (overlapColliders.Length == 0) {
#else
                var overlapColliders = Physics.OverlapSphereNonAlloc(availableLocations[index].position, m_Radius, m_HitColliders, m_CharacterLayer.value);
                if (overlapColliders == 0) {
#endif
                    ObjectPool.Return(availableLocations);
                    return availableLocations[index];
                }
                availableLocations.RemoveAt(index);
            }
            ObjectPool.Return(availableLocations);

            // There are no valid locations. Return the base location as a fallback.
            return base.GetSpawnLocationInternal();
        }

        /// <summary>
        /// Static method for returning the spawn locations.
        /// </summary>
        /// <param name="teamGame">Is the game a team game?</param>
        /// <param name="teamIndex">Specifies the index of spawn locations to retrieve if a team game.</param>
        public static Transform[] GetAllSpawnLocations(bool teamGame, int teamIndex)
        {
            return Instance.GetAllSpawnLocationsInternal(teamGame, teamIndex);
        }

        /// <summary>
        /// Internal method for returning the spawn locations.
        /// </summary>
        /// <param name="teamGame">Is the game a team game?</param>
        /// <param name="teamIndex">Specifies the index of spawn locations to retrieve if a team game.</param>
        private Transform[] GetAllSpawnLocationsInternal(bool teamGame, int teamIndex)
        {
            if (teamGame) {
                switch (teamIndex) {
                    case 0:
                        return m_FirstTeamSpawnLocations;
                    case 1:
                        return m_SecondTeamSpawnLocations;
                    case 2:
                        return m_ThirdTeamSpawnLocations;
                    case 3:
                        return m_FourthTeamSpawnLocations;
                }
                Debug.Log("Error: The team index is larger than the available spawn locations");
            }
            return m_SpawnLocations;
        }

        /// <summary>
        /// Draw an editor-only visualization of the spawn locations.
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            var blue = Color.blue;
            blue.a = 0.5f;
            Gizmos.color = blue;
            for (int i = 0; i < m_SpawnLocations.Length; ++i) {
                if (m_SpawnLocations[i] == null) {
                    continue;
                }
                Gizmos.DrawSphere(m_SpawnLocations[i].position, m_Radius);
            }
        }
    }
}