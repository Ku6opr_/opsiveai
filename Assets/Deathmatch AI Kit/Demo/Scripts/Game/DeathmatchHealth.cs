﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Extends the Third Person Controller Health component by notifying the Scoreboard of a death.
    /// </summary>
    public class DeathmatchHealth : CharacterHealth
    {
        /// <summary>
        /// Override Damage to add an extra damage amount depending on the bone hit.
        /// </summary>
        /// <param name="amount">The amount of damage taken.</param>
        /// <param name="position">The position of the damage.</param>
        /// <param name="force">The amount of force applied to the object while taking the damage.</param>
        /// <param name="radius">The radius of the explosive damage. If 0 then a non-exposive force will be used.</param>
        /// <param name="attacker">The GameObject that did the damage.</param>
        public override void Damage(float amount, Vector3 position, Vector3 force, float radius, GameObject attacker, GameObject hitGameObject)
        {
            // Do not allow friendly fire.
            if (DeathmatchManager.TeamGame && TeamManager.IsTeammate(m_GameObject, attacker)) {
                return;
            }

            base.Damage(amount, position, force, radius, attacker, hitGameObject);
        }

        /// <summary>
        /// The character has died. Report the death to interested components.
        /// </summary>
        /// <param name="force">The amount of force which killed the character.</param>
        /// <param name="position">The position of the force.</param>
        /// <param name="attacker">The GameObject that killed the character.</param>
        protected override void Die(Vector3 force, Vector3 position, GameObject attacker)
        {
            Scoreboard.ReportDeath(attacker, gameObject);
            if (TeamManager.IsInstantiated) {
                TeamManager.CancelBackupRequest(gameObject);
            }

            base.Die(force, position, attacker);
        }
    }
}