﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Activates the children upon start or pause. 
    /// This is a generic class which is good for activating or deactivating the UI at the start of the game or during a puse.
    /// </summary>
    public class Activator : MonoBehaviour
    {
        [Tooltip("Should the children be activated at game start")]
        [SerializeField] protected bool m_ActivateOnStart = true;
        [Tooltip("Should the children be activated or deactivated when the game is paused?")]
        [SerializeField] protected bool m_ActivateOnPause;

        /// <summary>
        /// Registers for any interested events and deactivates all of the children.
        /// </summary>
        private void Awake()
        {
            if (m_ActivateOnStart) {
                EventHandler.RegisterEvent("OnStartGame", StartGame);
            }
            if (m_ActivateOnPause) {
                EventHandler.RegisterEvent<bool>("OnPauseGame", PauseGame);
            }
            EventHandler.RegisterEvent("OnEventHandlerClear", EventHandlerClear);
            ActivateChildren(false);
        }

        /// <summary>
        /// The game has started. Activate the children.
        /// </summary>
        private void StartGame()
        {
            EventHandler.UnregisterEvent("OnStartGame", StartGame);

            ActivateChildren(true);
        }

        /// <summary>
        /// The game has been paused. Activator or deactivate the children.
        /// </summary>
        /// <param name="pause">Was the game paused?</param>
        private void PauseGame(bool pause)
        {
            ActivateChildren(pause);
        }

        /// <summary>
        /// Loops through the children and sets the active state.
        /// </summary>
        /// <param name="activate">Should the child GameObject be activated?</param>
        private void ActivateChildren(bool activate)
        {
            for (int i = 0; i < transform.childCount; ++i) {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(activate);
            }
        }

        /// <summary>
        /// The EventHandler was cleared. This will happen when a new scene is loaded. Unregister the registered events to prevent old events from being fired.
        /// </summary>
        private void EventHandlerClear()
        {
            if (m_ActivateOnPause) {
                EventHandler.UnregisterEvent<bool>("OnPauseGame", PauseGame);
            }
            EventHandler.UnregisterEvent("OnEventHandlerClear", EventHandlerClear);
        }
    }
}