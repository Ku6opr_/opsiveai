﻿using UnityEngine;
using UnityEngine.UI;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Sets the name of the player based off of the InputField.
    /// </summary>
    public class PlayerNameMonitor : MonoBehaviour
    {
        // Component references
        private InputField m_InputField;

        /// <summary>
        /// Cache the component references.
        /// </summary>
        private void Awake()
        {
            m_InputField = GetComponent<InputField>();
        }

        /// <summary>
        /// Assign the PlayerName to the input field value.
        /// </summary>
        private void Start()
        {
            DeathmatchManager.PlayerName = m_InputField.text;
        }

        /// <summary>
        /// The input field value has changed. Update the player name.
        /// </summary>
        public void InputEdited()
        {
            DeathmatchManager.PlayerName = m_InputField.text;
        }
    }
}