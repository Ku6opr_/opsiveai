﻿using UnityEngine;
using Opsive.ThirdPersonController.UI;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Inherits CrosshairsMonitor to expose the protected fields.
    /// </summary>
    public class DeathmatchCrosshairsMonitor : CrosshairsMonitor
    {
        // Exposed variables
        public LayerMask CrosshairsTargetLayer { set { m_CrosshairsTargetLayer = value; } }
    }
}