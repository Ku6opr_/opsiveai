﻿using UnityEngine;
using UnityEngine.UI;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Displays the local player's score as well as the high score.
    /// </summary>
    public class MiniScoreboardMonitor : MonoBehaviour
    {
        /// <summary>
        /// Contains the objects for showing the player score.
        /// </summary>
        [System.Serializable]
        protected class ScoreContainer
        {
            [Tooltip("A reference to the text which shows the player's name")]
            [SerializeField] protected Text m_Name;
            [Tooltip("A reference to the text which shows the player's score")]
            [SerializeField] protected Text m_Score;
            [Tooltip("A reference to the background whose color should change")]
            [SerializeField] protected Image m_Background;

            // Exposed properties
            public Text Name { get { return m_Name; } }
            public Text Score { get { return m_Score; } }
            public Image Background { get { return m_Background; } }
        }

        [Tooltip("References the local player's UI elements")]
        [SerializeField] protected ScoreContainer m_LocalPlayer;
        [Tooltip("References the leaders player's UI elements")]
        [SerializeField] protected ScoreContainer m_Leader;

        // Component references
        private Scoreboard m_Scoreboard;
        private GameObject m_Character;

        /// <summary>
        /// Register for any interested events.
        /// </summary>
        private void Awake()
        {
            EventHandler.RegisterEvent("OnScoreChange", ScoreChange);
            EventHandler.RegisterEvent<GameObject>("OnCameraAttachCharacter", AttachCharacter);
            EventHandler.RegisterEvent<bool>("OnShowUI", ShowUI);
            EventHandler.RegisterEvent("OnEventHandlerClear", EventHandlerClear);
            gameObject.SetActive(false);
        }

        /// <summary>
        /// The character has been attached to the camera. Update the UI reference and initialze the character-related values.
        /// </summary>
        /// <param name="character"></param>
        private void AttachCharacter(GameObject character)
        {
            m_Character = character;

            if (character == null) {
                // The object may be destroyed when Unity is ending.
                if (this != null) {
                    gameObject.SetActive(false);
                }
                return;
            }

            ScoreChange();
            gameObject.SetActive(true);
        }

        /// <summary>
        /// The score has changed - update the scoreboard UI.
        /// </summary>
        private void ScoreChange()
        {
            if (m_Scoreboard == null) {
                m_Scoreboard = Scoreboard.Instance;
            }

            // The character may not be assigned yet.
            if (m_Character == null) {
                return;
            }

            // Update the score.
            var colors = DeathmatchManager.TeamGame ? DeathmatchManager.PrimaryTeamColors : DeathmatchManager.PrimaryFFAColors;
            var localPlayerStats = m_Scoreboard.StatsForPlayer(m_Character);
            m_LocalPlayer.Name.text = localPlayerStats.Name;
            m_LocalPlayer.Score.text = localPlayerStats.Kills.ToString();
            m_LocalPlayer.Background.color = colors[localPlayerStats.TeamIndex];
            m_Leader.Name.text = m_Scoreboard.NonLocalPlayerLeader.Name;
            m_Leader.Score.text = m_Scoreboard.NonLocalPlayerLeader.Kills.ToString();
            m_Leader.Background.color = colors[m_Scoreboard.NonLocalPlayerLeader.TeamIndex];
        }

        /// <summary>
        /// Shows or hides the UI.
        /// </summary>
        /// <param name="show">Should the UI be shown?</param>
        private void ShowUI(bool show)
        {
            gameObject.SetActive(show);
        }

        /// <summary>
        /// The EventHandler was cleared. This will happen when a new scene is loaded. Unregister the registered events to prevent old events from being fired.
        /// </summary>
        private void EventHandlerClear()
        {
            EventHandler.UnregisterEvent("OnScoreChange", ScoreChange);
            EventHandler.UnregisterEvent<GameObject>("OnCameraAttachCharacter", AttachCharacter);
            EventHandler.UnregisterEvent<bool>("OnShowUI", ShowUI);
            EventHandler.UnregisterEvent("OnEventHandlerClear", EventHandlerClear);
        }
    }
}