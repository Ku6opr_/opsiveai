﻿using UnityEngine;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.UI;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Inherits HitIndicatorMonitor to hide the hit indicators when the game is over.
    /// </summary>
    public class DeathmatchHitIndicatorMonitor : HitIndicatorMonitor
    {
        /// <summary>
        /// Register for any interested events.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            EventHandler.RegisterEvent<bool>("OnGameOver", GameOver);
        }

        /// <summary>
        /// The game has ended. Deactivate the GameObject.
        /// </summary>
        /// <param name="winner">Did the local player win?</param>
        private void GameOver(bool winner)
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// The EventHandler was cleared. This will happen when a new scene is loaded. Unregister the registered events to prevent old events from being fired.
        /// </summary>
        protected override void EventHandlerClear()
        {
            base.EventHandlerClear();

            EventHandler.UnregisterEvent<bool>("OnGameOver", GameOver);
        }
    }
}