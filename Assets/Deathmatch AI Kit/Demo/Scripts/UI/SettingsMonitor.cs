﻿using UnityEngine;
using UnityEngine.UI;

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// The SettingsMonitor will manage the UI elements for the deathmatch game settings.
    /// </summary>
    public class SettingsMonitor : MonoBehaviour
    {
        [Tooltip("Should the UI element be disabled if the game mode is a FFA game?")]
        [SerializeField] protected bool m_DisableWithFFAMode = true;
        [Tooltip("Should the UI element be disabled if the game mode is a team game?")]
        [SerializeField] protected bool m_DisableWithTeamMode;
        [Tooltip("Should the image be updated with the mode type?")]
        [SerializeField] protected bool m_UpdateModeTypeImage;
        [Tooltip("If Update Mode Type Image is enabled, the sprite that should be used for a FFA game")]
        [SerializeField] protected Sprite m_FFASprite;
        [Tooltip("If Update Mode Type Image is enabled, the sprite that should be used for a team game")]
        [SerializeField] protected Sprite m_TeamSprite;
        [Tooltip("If Update Mode Type Image is enabled, the image color that should be used for a FFA game")]
        [SerializeField] protected Color m_FFASpriteColor = Color.white;
        [Tooltip("If Update Mode Type Image is enabled, the image color that should be used for a team game")]
        [SerializeField] protected Color m_TeamSpriteColor = Color.white;
        [Tooltip("Should the text color be updated with the mode type?")]
        [SerializeField] protected bool m_UpdateModeTypeTextColor;
        [Tooltip("If Update Mode Text Color is enabled, the text color that should be used for a FFA game")]
        [SerializeField] protected Color m_FFATextColor = Color.white;
        [Tooltip("If Update Mode Text Color is enabled, the text color that should be used for a team game")]
        [SerializeField] protected Color m_TeamTextColor = Color.white;
        [Tooltip("The difficulty that the button represents")]
        [SerializeField] protected DeathmatchManager.AIDifficulty m_Difficulty;
        [Tooltip("Should the image be updated with the mode type?")]
        [SerializeField] protected bool m_UpdateDifficultyImage;
        [Tooltip("If Update Difficulty Image is enabled, the sprite that should be used when the current difficulty is selected")]
        [SerializeField] protected Sprite m_SelectedSprite;
        [Tooltip("If Update Difficulty Image is enabled, the sprite that should be used when the current difficulty is not selected")]
        [SerializeField] protected Sprite m_NotSelectedSprite;
        [Tooltip("If Update Difficulty Image is enabled, the image color that should be used when the current difficulty is selected")]
        [SerializeField] protected Color m_SelectedSpriteColor = Color.white;
        [Tooltip("If Update Difficulty Image is enabled, the image color that should be used when the current difficulty is not selected")]
        [SerializeField] protected Color m_NotSelectedSpriteColor = Color.white;
        [Tooltip("Should the text color be updated with the difficulty?")]
        [SerializeField] protected bool m_UpdateDifficultyTextColor;
        [Tooltip("If Update Difficulty Text Color is enabled, the text color that should be used when the current difficulty is selected")]
        [SerializeField] protected Color m_SelectedTextColor = Color.white;
        [Tooltip("If Update Difficulty Text Color is enabled, the text color that should be used when the current difficulty is not selected")]
        [SerializeField] protected Color m_NotSelectedTextColor = Color.white;
        [Tooltip("Should the text be updated with the FFA or team count?")]
        [SerializeField] protected bool m_UpdateCount;
        [Tooltip("Should the text be updated with the team count?")]
        [SerializeField] protected bool m_UpdateTeamCount;

        // Component references
        private GameObject m_GameObject;
        private Text m_Text;
        private Image m_Image;

        /// <summary>
        /// Initailize the component references.
        /// </summary>
        private void Awake()
        {
            m_GameObject = gameObject;
            if (m_UpdateModeTypeImage || m_UpdateDifficultyImage) {
                m_Image = GetComponent<Image>();
            }
            if (m_UpdateModeTypeTextColor || m_UpdateDifficultyTextColor || m_UpdateCount || m_UpdateTeamCount) {
                m_Text = GetComponent<Text>();
            }
        }

        /// <summary>
        /// Initialize the text.
        /// </summary>
        private void Start()
        {
            ModeSwitched();
        }

        /// <summary>
        /// The mode has been switched between FFA and Team.
        /// </summary>
        public void ModeSwitched()
        {
            if (m_DisableWithFFAMode) {
                m_GameObject.SetActive(DeathmatchManager.TeamGame);
            } else if (m_DisableWithTeamMode) {
                m_GameObject.SetActive(!DeathmatchManager.TeamGame);
            }
            UpdateUI();
        }

        /// <summary>
        /// Update the UI to reflect the current values.
        /// </summary>
        public void UpdateUI()
        {
            if (m_UpdateModeTypeImage) {
                m_Image.sprite = DeathmatchManager.TeamGame ? m_TeamSprite : m_FFASprite;
                m_Image.color = DeathmatchManager.TeamGame ? m_TeamSpriteColor : m_FFASpriteColor;
            }
            if (m_UpdateDifficultyImage) {
                m_Image.sprite = DeathmatchManager.Difficulty == m_Difficulty ? m_SelectedSprite : m_NotSelectedSprite;
                m_Image.color = DeathmatchManager.Difficulty == m_Difficulty ? m_SelectedSpriteColor : m_NotSelectedSpriteColor;
            }
            if (m_UpdateModeTypeTextColor) {
                m_Text.color = DeathmatchManager.TeamGame ? m_TeamTextColor : m_FFATextColor;
            }
            if (m_UpdateDifficultyTextColor) {
                m_Text.color = DeathmatchManager.Difficulty == m_Difficulty ? m_SelectedTextColor : m_NotSelectedTextColor;
            }
            if (m_UpdateCount) {
                m_Text.text = (DeathmatchManager.TeamGame ? DeathmatchManager.PlayersPerTeam : DeathmatchManager.PlayerCount).ToString();
            }
            if (m_UpdateTeamCount) {
                m_Text.text = DeathmatchManager.TeamCount.ToString();
            }
        }
    }
}