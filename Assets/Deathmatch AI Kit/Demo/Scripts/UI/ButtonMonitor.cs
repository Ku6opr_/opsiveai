﻿using UnityEngine;
#if !(UNITY_5_1 || UNITY_5_2)
using UnityEngine.SceneManagement;
#endif

namespace Opsive.DeathmatchAIKit.UI
{
    /// <summary>
    /// Receives the button events.
    /// </summary>
    public class ButtonMonitor : MonoBehaviour
    {
        /// <summary>
        /// Toggles the deathmatch game mode.
        /// </summary>
        public void ToggleMode()
        {
            DeathmatchManager.TeamGame = !DeathmatchManager.TeamGame;
        }

        /// <summary>
        /// Changes the number of FFA players.
        /// </summary>
        /// <param name="increase">Should the count be increased?</param>
        public void ChangePlayerCount(bool increase)
        {
            DeathmatchManager.PlayerCount += (increase ? 1 : -1);
        }

        /// <summary>
        /// Changes the number of team players.
        /// </summary>
        /// <param name="increase">Should the count be increased?</param>
        public void ChangePlayersPerTeam(bool increase)
        {
            DeathmatchManager.PlayersPerTeam += (increase ? 1 : -1);
        }

        /// <summary>
        /// Changes the number of teams.
        /// </summary>
        /// <param name="increase">Should the count be increased?</param>
        public void ChangeTeamCount(bool increase)
        {
            DeathmatchManager.TeamCount += (increase ? 1 : -1);
        }

        /// <summary>
        /// Changes the AI difficulty level.
        /// </summary>
        /// <param name="level">The level of difficulty.</param>
        public void ChangeDifficulty(int level)
        {
            DeathmatchManager.Difficulty = (DeathmatchManager.AIDifficulty)level;
        }

        /// <summary>
        /// Starts the game by loading the first scene.
        /// </summary>
        public void StartGame()
        {
#if !(UNITY_5_1 || UNITY_5_2)
            SceneManager.LoadScene(1);
#else
            Application.LoadLevel(1);
#endif
        }

        /// <summary>
        /// Pauses or unpauses the game.
        /// </summary>
        /// <param name="paused">True if the game is paused.</param>
        public void PauseGame(bool paused)
        {
            DeathmatchManager.Paused = paused;
        }

        /// <summary>
        /// Ends the game.
        /// </summary>
        public void EndGame()
        {
            DeathmatchManager.EndGame();
        }
    }
}