﻿using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Abilities;

namespace Opsive.DeathmatchAIKit.Abilities
{
    /// <summary>
    /// The Use Air Lift ability will allow the character to use a Deathmatch Kit air lift.
    /// </summary>
    public class UseAirLift : Ability
    {
        // Internal variables
        private bool m_Grounded;

        /// <summary>
        /// Starts executing the ability.
        /// </summary>
        protected override void AbilityStarted()
        {
            m_Grounded = true;
            base.AbilityStarted();
            EventHandler.RegisterEvent<bool>(m_GameObject, "OnControllerGrounded", OnGrounded);
        }

        /// <summary>
        /// Unregister for any events that the ability was registered for.
        /// </summary>
        protected override void AbilityStopped()
        {
            base.AbilityStopped();
            EventHandler.UnregisterEvent<bool>(m_GameObject, "OnControllerGrounded", OnGrounded);
        }

        /// <summary>
        /// Returns the destination state for the given layer.
        /// </summary>
        /// <param name="layer">The Animator layer index.</param>
        /// <returns>The state that the Animator should be in for the given layer. An empty string indicates no change.</returns>
        public override string GetDestinationState(int layer)
        {
            // The ability only affects the base and upper layers.
            if (m_Grounded || (layer != m_AnimatorMonitor.BaseLayerIndex && layer != m_AnimatorMonitor.UpperLayerIndex && !m_AnimatorMonitor.ItemUsesAbilityLayer(this, layer))) {
                return string.Empty;
            }

            return "Air Lift.Use";
        }

        /// <summary>
        /// Can the ability be stopped?
        /// </summary>
        /// <returns>True if the ability can be stopped.</returns>
        public override bool CanStopAbility()
        {
            return m_Grounded && m_Controller.Velocity.y < -0.5f;
        }

        /// <summary>
        /// The character has changed grounded state. 
        /// </summary>
        /// <param name="grounded">Is the character on the ground?</param>
        private void OnGrounded(bool grounded)
        {
            m_Grounded = grounded;
            if (!grounded) {
                m_AnimatorMonitor.DetermineStates();
            }
        }
    }
}