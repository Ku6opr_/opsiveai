﻿using UnityEngine;
#if !(UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4)
using UnityEngine.AI;
#endif
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI
{
    /// <summary>
    /// Extends the NavMeshAgentBridge to allow the agent to navigate DeathmatchAIKit air lifts.
    /// </summary>
    public class NavMeshAgentDeathmatchBridge : NavMeshAgentBridge
    {
        [Tooltip("Start moving forward on the air lift when the vertical velocity is less than the specified amount")]
        [SerializeField] protected float m_MoveVerticalVelocityThreshold = 2;

        // Internal variables
        private int m_AirLiftAreaIndex;

        /// <summary>
        /// Initialize the default values.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            m_AirLiftAreaIndex = NavMesh.GetAreaFromName("AirLift");
        }

        /// <summary>
        /// Updates the velocity and look rotation using the off mesh link.
        /// </summary>
        /// <param name="velocity">The desired velocity.</param>
        /// <param name="lookRotation">The desired look rotation.</param>
        protected override void UpdateOffMeshLink(ref Vector3 velocity, ref Quaternion lookRotation)
        {
            if (m_NavMeshAgent.currentOffMeshLinkData.linkType == OffMeshLinkType.LinkTypeManual) {
                var offMeshLink = m_NavMeshAgent.currentOffMeshLinkData.offMeshLink;
                // The lift area requires special handling. While on the ground move towards the start point. When the agent has moved up the lift the agent should start
                // to exit the lift.
                if (offMeshLink.area == m_AirLiftAreaIndex) {
                    if (m_Controller.Grounded) {
                        // Move towards the start position to get on the lift.
                        velocity = m_Transform.InverseTransformPoint(m_NavMeshAgent.currentOffMeshLinkData.startPos);
                        velocity.y = 0;
                        velocity.Normalize();

                        // Face in the direction of the end position so the agent is ready to exit the lift.
                        var direction = m_NavMeshAgent.currentOffMeshLinkData.endPos - m_Transform.position;
                        direction.y = 0;
                        lookRotation = Quaternion.LookRotation(direction);
                    } else if (m_Controller.Velocity.y < m_MoveVerticalVelocityThreshold) {
                        // Start moving towards the end position as soon as the vertical velocity is below a threshold.
                        velocity = m_Transform.InverseTransformPoint(m_NavMeshAgent.currentOffMeshLinkData.endPos);
                        velocity.y = 0;
                        // Don't let the agent move backwards to hit the end position.
                        if (velocity.z < 0) {
                            velocity.z = 0;
                        }
                        velocity.Normalize();
                    }
                    return;
                }
            }

            // The character is not on an air lift so the base class can handle it.
            base.UpdateOffMeshLink(ref velocity, ref lookRotation);
        }

        /// <summary>
        /// The character has changed grounded state. 
        /// </summary>
        /// <param name="grounded">Is the character on the ground?</param>
        protected override void OnGrounded(bool grounded)
        {
            if (grounded) {
                // The agent is no longer on an off mesh link if they just landed.
                if (m_NavMeshAgent.isOnOffMeshLink && m_NavMeshAgent.currentOffMeshLinkData.linkType == OffMeshLinkType.LinkTypeManual &&
                    m_NavMeshAgent.currentOffMeshLinkData.offMeshLink.area == m_AirLiftAreaIndex) {
                    m_NavMeshAgent.CompleteOffMeshLink();
                }
            }

            base.OnGrounded(grounded);
        }
    }
}