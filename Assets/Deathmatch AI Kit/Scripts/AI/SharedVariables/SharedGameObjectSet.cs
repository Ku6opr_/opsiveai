﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

namespace Opsive.DeathmatchAIKit.AI
{
    [System.Serializable]
    public class SharedGameObjectSet : SharedVariable<HashSet<GameObject>>
    {
        public SharedGameObjectSet() { mValue = new HashSet<GameObject>(); }
        public static implicit operator SharedGameObjectSet(HashSet<GameObject> value) { return new SharedGameObjectSet { mValue = value }; }
    }
}