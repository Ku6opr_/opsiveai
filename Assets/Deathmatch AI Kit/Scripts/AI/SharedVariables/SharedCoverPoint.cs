using BehaviorDesigner.Runtime;

namespace Opsive.DeathmatchAIKit.AI
{
    [System.Serializable]
    public class SharedCoverPoint : SharedVariable<CoverPoint>
    {
        public static implicit operator SharedCoverPoint(CoverPoint value) { return new SharedCoverPoint { mValue = value }; }
    }
}