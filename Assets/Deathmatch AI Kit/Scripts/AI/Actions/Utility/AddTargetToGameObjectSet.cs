﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Adds the target parent to the GameObject Set.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class AddTargetToGameObjectSet : Action
    {
        [Tooltip("The value to add to the SharedGameObjectList.")]
        public SharedGameObject target;
        [RequiredField]
        [Tooltip("The SharedGameObjectSet to add to")]
        public SharedGameObjectSet targetVariable;

        /// <summary>
        /// Adds the target to the GameObjectSet. 
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
            // Use the target parent instead of the direct GameObject.
            targetVariable.Value.Add(Utility.GetComponentForType<Animator>(target.Value, true).gameObject);

            return TaskStatus.Success;
        }

        /// <summary>
        /// Reset the Behavior Designer variables.
        /// </summary>
        public override void OnReset()
        {
            target = null;
            targetVariable = null;
        }
    }
}