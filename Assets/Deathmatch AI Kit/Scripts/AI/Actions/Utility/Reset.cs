using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Resets the variables.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class Reset : Action
    {
        [Tooltip("The GameObject to attack")]
        public SharedGameObject m_Target;
        [Tooltip("The possible GameObject to attack")]
        public SharedGameObject m_PossibleTarget;
        [Tooltip("A set of targets that the agent should ignore")]
        public SharedGameObjectSet m_IgnoreTargets;
        [Tooltip("The GameObject which the agent is using as cover")]
        public SharedCoverPoint m_CoverPoint;
        [Tooltip("The leader of the group")]
        public SharedGameObject m_Leader;
        [Tooltip("The teammate requesting backup")]
        public SharedGameObject m_BackupRequestor;
        [Tooltip("The target of the teammate who is requesting backup")]
        public SharedGameObject m_BackupTarget;
        [Tooltip("A bool representing if the agent should search for the target")]
        public SharedBool m_Search;
        [Tooltip("A bool representing if the agent can attack the target")]
        public SharedBool m_CanAttack;
        [Tooltip("Should the cancel backup request be sent?")]
        public SharedBool m_CancelBackupRequest;

        // Component references
        private DeathmatchAgent m_DeathmatchAgent;

        /// <summary>
        /// Cache the component references.
        /// </summary>
        public override void OnAwake()
        {
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();
        }

        /// <summary>
        /// Reset the SharedVariables.
        /// </summary>
        /// <returns>Always returns Success.</returns>
        public override TaskStatus OnUpdate()
        {
            if (!m_Target.IsNone) {
                m_Target.Value = null;
                m_DeathmatchAgent.Target = null;
            }
            if (!m_PossibleTarget.IsNone) {
                m_PossibleTarget.Value = null;
            }
            if (!m_IgnoreTargets.IsNone) {
                m_IgnoreTargets.Value.Clear();
            }
            if (!m_CoverPoint.IsNone) {
                if (m_CoverPoint.Value != null) {
                    m_CoverPoint.Value.Occupant = null;
                }
                m_CoverPoint.Value = null;
            }
            if (!m_Leader.IsNone) {
                m_Leader.Value = null;
            }
            if (!m_BackupRequestor.IsNone) {
                m_BackupRequestor.Value = null;
            }
            if (!m_BackupTarget.IsNone) {
                m_BackupTarget.Value = null;
            }
            if (!m_Search.IsNone) {
                m_Search.Value = false;
            }
            if (!m_CanAttack.IsNone) {
                m_CanAttack.Value = true;
            }
            if (TeamManager.IsInstantiated && m_CancelBackupRequest.Value) {
                TeamManager.CancelBackupRequest(gameObject);
            }

            return TaskStatus.Success;
        }
    }
}