using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Updates the backup request with a new target.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class UpdateBackupRequest : Action
    {
        [Tooltip("The GameObject to attack")]
        [SerializeField] SharedGameObject m_Target;

        /// <summary>
        /// Update the backup request.
        /// </summary>
        /// <returns>Always returns Success.</returns>
        public override TaskStatus OnUpdate()
        {
            TeamManager.UpdateBackupRequest(gameObject, Utility.GetComponentForType<Animator>(m_Target.Value, true).gameObject);

            return TaskStatus.Success;
        }
    }
}