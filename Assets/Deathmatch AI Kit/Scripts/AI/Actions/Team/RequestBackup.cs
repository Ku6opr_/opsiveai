using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Requests backup from other teammates.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class RequestBackup : Action
    {
        [Tooltip("The GameObject to attack")]
        [SerializeField] SharedGameObject m_Target;

        /// <summary>
        /// Request backup from the agent's teammates.
        /// </summary>
        /// <returns>Always returns Success.</returns>
        public override TaskStatus OnUpdate()
        {
            TeamManager.RequestBackup(gameObject, Utility.GetComponentForType<Animator>(m_Target.Value, true).gameObject);

            return TaskStatus.Success;
        }
    }
}