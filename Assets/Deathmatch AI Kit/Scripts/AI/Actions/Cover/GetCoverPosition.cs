﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Gets the cover position from a cover point.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class GetCoverPosition : Action
    {
        [Tooltip("The current cover point")]
        [SerializeField] protected SharedCoverPoint m_CoverPoint;
        [Tooltip("The position of the found CoverPoint")]
        [SharedRequired] [SerializeField] protected SharedVector3 m_CoverPosition;

        /// <summary>
        /// Set the cover point to null and return success.
        /// </summary>
        /// <returns>True if a cover point was found.</returns>
        public override TaskStatus OnUpdate()
        {
            m_CoverPosition.Value = m_CoverPoint.Value.transform.position;
            return TaskStatus.Success;
        }
    }
}