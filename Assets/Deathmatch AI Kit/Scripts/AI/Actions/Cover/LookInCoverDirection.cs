using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Opsive.ThirdPersonController;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Look in the same direction as the cover point.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class LookInCoverDirection : Action
    {
        [Tooltip("The agent is done rotating when the angle is less than this value")]
        [SerializeField] protected SharedFloat m_RotationEpsilon = 0.5f;
        [Tooltip("The cover point to look in the direction of")]
        [SerializeField] protected SharedCoverPoint m_CoverPoint;

        // Component references
        private RigidbodyCharacterController m_Controller;

        /// <summary>
        /// Cache the component references.
        /// </summary>
        public override void OnAwake()
        {
            m_Controller = GetComponent<RigidbodyCharacterController>();
        }

        /// <summary>
        /// Looks at the target position.
        /// </summary>
        /// <returns>Success if the agent is looking at the target position.</returns>
        public override TaskStatus OnUpdate()
        {
            var rotation = Quaternion.LookRotation(m_CoverPoint.Value.transform.forward);
            // Return a task status of success once the agent is done rotating.
            if (Quaternion.Angle(transform.rotation, rotation) < m_RotationEpsilon.Value) {
                return TaskStatus.Success;
            }
            // The agent hasn't reached the target yet so keep rotating towards it.
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, m_Controller.RotationSpeed);
            return TaskStatus.Running;
        }

        /// <summary>
        /// Resets the public variables.
        /// </summary>
        public override void OnReset()
        {
            m_RotationEpsilon = 0.5f;
            m_CoverPoint = null;
        }
    }
}