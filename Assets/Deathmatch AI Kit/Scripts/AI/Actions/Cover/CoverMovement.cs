﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Abilities;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Manages movement while in cover.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class CoverMovement : Action
    {
        [Tooltip("The target GameObject")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The minimum amount of time that the agent can wait to aim while in cover")]
        [SerializeField] protected SharedFloat m_MinWaitDuration;
        [Tooltip("The maximum amount of time that the agent can wait to aim while in cover")]
        [SerializeField] protected SharedFloat m_MaxWaitDuration;
        [Tooltip("The minimum amount of time that the agent can aim while in cover")]
        [SerializeField] protected SharedFloat m_MinAimDuration;
        [Tooltip("The maximum amount of time that the agent can aim while in cover")]
        [SerializeField] protected SharedFloat m_MaxAimDuration;
        [Tooltip("The point to take cover at")]
        [SharedRequired] [SerializeField] protected SharedCoverPoint m_CoverPoint;
        [Tooltip("Can the agent attack?")]
        [SharedRequired] [SerializeField] protected SharedBool m_CanAttack;

        // Internal variables
        private bool m_NullTarget;
        private float m_LastAimTime;
        private float m_LastWaitTime;
        private float m_Duration;

        // SharedFields
        private SharedProperty<Item> m_CurrentPrimaryItem = null;
        
        // Component references
        private RigidbodyCharacterController m_Controller;
        private AnimatorMonitor m_AnimatorMonitor;
        private Cover m_Cover;
        private DeathmatchAgent m_DeathmatchAgent;

        /// <summary>
        /// Cache the component references and register for any interested events.
        /// </summary>
        public override void OnAwake()
        {
            m_Controller = GetComponent<RigidbodyCharacterController>();
            m_AnimatorMonitor = GetComponent<AnimatorMonitor>();
            m_Cover = GetComponent<Cover>();
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();
            EventHandler.RegisterEvent<float, Vector3, Vector3, GameObject>(gameObject, "OnHealthDamageDetails", OnDamage);
            SharedManager.InitializeSharedFields(gameObject, this);
        }

        /// <summary>
        /// Start the cover ability.
        /// </summary>
        public override void OnStart()
        {
            m_Cover.PredeterminedCoverPoint = m_CoverPoint.Value.transform.position;
            // Cover may already be active if the task is being started after a new target is within sight.
            if (!m_Cover.IsActive) {
                m_Controller.TryStartAbility(m_Cover);
            }

            // Initialize the default values.
            m_Duration = Random.Range(m_MinWaitDuration.Value, m_MaxWaitDuration.Value);
            m_LastWaitTime = Time.time;
            m_LastAimTime = -Mathf.Max(m_MaxWaitDuration.Value, m_MaxAimDuration.Value);
            m_NullTarget = m_Target.Value == null;

            // The agent should not aim immediately after taking cover.
            m_CanAttack.Value = m_Controller.Aim = false;

            // The task should be notified when the character is popped from cover.
            EventHandler.RegisterEvent(gameObject, "OnAnimatorPoppedFromCover", PoppedFromCover);
        }

        /// <summary>
        /// Move between an aiming and non-aiming state.
        /// </summary>
        /// <returns>Always returns a status of Running - this task must be interrupted.</returns>
        public override TaskStatus OnUpdate()
        {
            // Wait to move within cover until the agent is in cover position.
            if (m_Cover.CurrentCoverID == Cover.CoverIDs.None) {
                return TaskStatus.Running;
            }

            // If the target is null then keep waiting for a target to be in sight of the cover point.
            if (m_Target.Value == null) {
                return TaskStatus.Running;
            }

            // Break from cover if the target is no longer in sight.
            if (m_DeathmatchAgent.LineOfSight(Utility.GetComponentForType<Animator>(m_Target.Value, true).transform, false) == null) {
                return TaskStatus.Failure;
            }

            var coverPosition = m_CoverPoint.Value.transform.position;
            coverPosition.y = transform.position.y;
            var difference = transform.InverseTransformPoint(coverPosition);
            if (m_Controller.Aim || Mathf.Abs(difference.x) < 0.05f) {
                // Alternate between aiming and non-aiming. Don't aim if the character is reloading.
                var primaryItem = m_CurrentPrimaryItem.Get();
                var isReloading = false;
                if (primaryItem is IReloadableItem) {
                    isReloading = (primaryItem as IReloadableItem).IsReloading();
                }
                if (m_Controller.Aim) {
                    if (m_LastAimTime + m_Duration < Time.time || isReloading) {
                        m_CanAttack.Value = m_Controller.Aim = false;
                        m_LastWaitTime = Time.time;
                        m_Duration = Random.Range(m_MinWaitDuration.Value, m_MaxWaitDuration.Value);
                    }
                } else {
                    if (m_LastWaitTime + m_Duration < Time.time && !isReloading) {
                        m_Controller.Aim = true;
                        m_LastAimTime = Time.time;
                        m_Duration = Random.Range(m_MinAimDuration.Value, m_MaxAimDuration.Value);
                    }
                }
                m_AnimatorMonitor.SetHorizontalInputValue(0, 0);
            } else {
                // Strafe if the agent isn't precisely over the cover point.
                m_AnimatorMonitor.SetHorizontalInputValue(Mathf.Abs(difference.x) > 0.1f ? Mathf.Sign(difference.x) : difference.x * 2);
            }
            return TaskStatus.Running;
        }

        /// <summary>
        /// The agent has finished popping from cover.
        /// </summary>
        private void PoppedFromCover()
        {
            m_CanAttack.Value = true;
        }

        /// <summary>
        /// Stop the cover ability.
        /// </summary>
        public override void OnEnd()
        {
            // The Cover ability shouldn't be stopped if the target was null but is not anymore. This allows the agent to be in cover without a target
            // and then switch to a new Cover task without having to start and stop the Cover ability.
            if (!m_NullTarget || m_Target.Value == null) {
                m_Controller.TryStopAbility(m_Cover);
            }
            m_Controller.Aim = false;
            EventHandler.UnregisterEvent(gameObject, "OnAnimatorPoppedFromCover", PoppedFromCover);
        }

        /// <summary>
        /// The agent has taken damage. Stop taking cover to avoid fire.
        /// </summary>
        /// <param name="amount">The amount of damage received.</param>
        /// <param name="position">The position of the damage.</param>
        /// <param name="force">The force of the damage.</param>
        /// <param name="attacker">The GameObject that attacked the agent.</param>
        private void OnDamage(float amount, Vector3 position, Vector3 force, GameObject attacker)
        {
            m_Controller.Aim = false;
            m_LastWaitTime = Time.time;
            m_Duration = Random.Range(m_MinWaitDuration.Value, m_MaxWaitDuration.Value);
        }
    }
}