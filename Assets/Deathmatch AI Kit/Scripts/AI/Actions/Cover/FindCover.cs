﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Abilities;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Finds an unoccupied cover point.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class FindCover : Action
    {
        [Tooltip("The GameObject to attack")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The found CoverPoint")]
        [SerializeField] protected SharedCoverPoint m_CoverPoint;

        // Internal variables
        private CoverPoint[] m_CoverPoints;

        // Component references
        private RigidbodyCharacterController m_Controller;
        private Cover m_Cover;

        /// <summary>
        /// Cache the component references and initialize the default values.
        /// </summary>
        public override void OnAwake()
        {
            base.OnAwake();

            m_Controller = GetComponent<RigidbodyCharacterController>();
            m_Cover = GetComponent<Cover>();
            m_CoverPoints = GameObject.FindObjectsOfType<CoverPoint>();
        }

        /// <summary>
        /// Find an unoccupied cover point. Returns success if a cover point was found.
        /// </summary>
        /// <returns>True if a cover point was found.</returns>
        public override TaskStatus OnUpdate()
        {
            var distace = float.MaxValue;
            float localDistance;

            CoverPoint[] coverPoints;
            // If the agent is going into cover for the first time then find the closest unoccupied cover point. Otherwise, find the closest linked cover point.
            if (m_CoverPoint.Value == null || m_CoverPoint.Value.LinkedCoverPoints.Length == 0) {
                coverPoints = m_CoverPoints;
            } else {
                coverPoints = m_CoverPoint.Value.LinkedCoverPoints;
            }
            for (int i = 0; i < coverPoints.Length; ++i) {
                if (coverPoints[i].gameObject.activeInHierarchy && coverPoints[i].IsValidCoverPoint(transform, m_Target.Value == null ? null : m_Target.Value.transform)) {
                    if ((localDistance = (transform.position - coverPoints[i].transform.position).sqrMagnitude) < distace) {
                        distace = localDistance;
                        m_CoverPoint.Value = coverPoints[i];
                    }
                }
            }
            // If the distance is still the max value then a new cover point wasn't found.
            if (distace == float.MaxValue) {
                m_CoverPoint.Value = null;
                // The CoverMovement task will not stop the ability if there is a target.
                m_Controller.TryStopAbility(m_Cover);
                return TaskStatus.Failure;
            }

            // A cover point was found. Return success.
            m_CoverPoint.Value.Occupant = transform;
            return TaskStatus.Success;
        }
    }
}