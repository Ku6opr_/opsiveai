﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using BehaviorDesigner.Runtime.Tasks.ThirdPersonController;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Attacks the target. The agent will only attack if the target is within sight.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class Attack : Action
    {
        [Tooltip("The ItemType to use. The item must be equipped. If null the primaryItem value will be used.")]
        [SerializeField] protected SharedItemType m_ItemType;
        [Tooltip("The maximum field of view that the agent can see other targets (in degrees)")]
        [SerializeField] protected SharedFloat m_FieldOfView = 90;
        [Tooltip("The GameObject to attack")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("Is the target within sight?")]
        [SerializeField] protected SharedBool m_TargetInSight;

        // SharedFields
        private SharedProperty<Item> m_CurrentSecondaryItem = null;

        // Component references
        private ItemHandler m_ItemHandler;
        private DeathmatchAgent m_DeathmatchAgent;

        /// <summary>
        /// Cache the component references and initialize the SharedFields.
        /// </summary>
        public override void OnAwake()
        {
            m_ItemHandler = GetComponent<ItemHandler>();
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();

            SharedManager.InitializeSharedFields(gameObject, this);
        }

        /// <summary>
        /// Perform the attack.
        /// </summary>
        /// <returns>Success if the target was attacked.</returns>
        public override TaskStatus OnUpdate()
        {
            // Don't attack if the target isn't in sight.
            var targetParent = Utility.GetComponentForType<Animator>(m_Target.Value, true).transform;
            if (!m_DeathmatchAgent.TargetInSight(targetParent, m_FieldOfView.Value)) {
                // Reset the SharedBool and the Target Transform of the Deathmatch Agent. This will stop the agent from looking at the target. 
                m_TargetInSight.Value = false;
                m_DeathmatchAgent.Target = null;
                return TaskStatus.Failure;
            }

            // The target should be attacked. Try to use the item.
            m_TargetInSight.Value = true;
            m_DeathmatchAgent.Target = m_Target.Value;

            // If the item is a grenade then the throw force should be based off of the target distance.
            if (m_DeathmatchAgent.WeaponStatForItemType(m_ItemType.Value).Class == DeathmatchAgent.WeaponStat.WeaponClass.Grenade) {
                var throwableItem = m_CurrentSecondaryItem.Get() as DeathmatchThrowableItem;
                if (throwableItem  != null) {
                    var force = throwableItem.ThrowForce;
                    force.z = Vector3.Distance(targetParent.position, transform.position) * 1.5f;
                    throwableItem.ThrowForce = force;
                }
            }

            return m_ItemHandler.TryUseItem(m_ItemType.Value.GetType()) ? TaskStatus.Success : TaskStatus.Failure;
        }

        /// <summary>
        /// Reset the Behavior Designer variables.
        /// </summary>
        public override void OnReset()
        {
            m_ItemType = null;
            m_Target = null;
            m_TargetInSight = false;
        }
    }
}