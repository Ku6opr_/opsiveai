﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.ThirdPersonController;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;
using System.Collections.Generic;
using WeaponClass = Opsive.DeathmatchAIKit.AI.DeathmatchAgent.WeaponStat.WeaponClass;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Determines the best weapon to be used against the target.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class DetermineWeapon : Action
    {
        [Tooltip("Specifies the target to attack")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The agent has low health when the health is lower than this value")]
        [SerializeField] protected SharedFloat m_LowHealth;
        [Tooltip("The size of the radius to determine if the target is within a group")]
        [SerializeField] protected SharedFloat m_GroupRadius = 5;
        [Tooltip("The number of targets which must be within the radius in order to be considered a group")]
        [SerializeField] protected SharedInt m_GroupCount = 2;
        [Tooltip("Each available weapon is going to be checked to determine its use likelihood. Switch weapons if the use likelihood is greater than the specified value")]
        [SerializeField] protected SharedFloat m_LikelihoodAdvantage = 1;
        [Tooltip("Specifies the amount of time to wait before selecting the grenade again after throwing it")]
        [SerializeField] protected SharedFloat m_GrenadeTimeout = 4;
        [Tooltip("Specifies the amount of time to wait before selecting the grenade again after shooting it")]
        [SerializeField] protected SharedFloat m_RocketTimeout = 2;
        [Tooltip("The ItemType to switch to")]
        [RequiredField] [SerializeField] protected SharedItemType m_TargetItemType;

        // Internal variables
        private List<ItemType> m_ItemTypes = new List<ItemType>();
        private List<DeathmatchAgent.WeaponStat> m_WeaponPool = new List<DeathmatchAgent.WeaponStat>();
        private SharedProperty<float> m_CurrentHealth = null;
        private Collider[] m_HitColliders;
        private float m_GrenadeThrowTime;
        private float m_RocketFireTime;

        // Component references
        private DeathmatchAgent m_DeathmatchAgent;
        private Inventory m_Inventory;

        /// <summary>
        /// Cache the component references and initialize the SharedFields.
        /// </summary>
        public override void OnAwake()
        {
            m_Inventory = GetComponent<Inventory>();
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();
#if !(UNITY_5_1 || UNITY_5_2)
            m_HitColliders = new Collider[m_GroupCount.Value + 1];
#endif
            m_GrenadeThrowTime = -m_GrenadeTimeout.Value;
            m_RocketFireTime = -m_RocketTimeout.Value;

            SharedManager.InitializeSharedFields(gameObject, this);

            EventHandler.RegisterEvent(gameObject, "OnThrowItem", ThrewGrenade);
            EventHandler.RegisterEvent(gameObject, "OnFireProjectile", FiredRocket);
        }

        /// <summary>
        /// Determines which weapon to switch to. Returns Success if a new weapon should be switched to.
        /// </summary>
        /// <returns>Success if a new weapon should be switched to.</returns>
        public override TaskStatus OnUpdate()
        {
            // Get a list of all weapons that the character currently has.
            m_Inventory.GetAvailableItems(ref m_ItemTypes);

            // Initially the character can use any available weapon.
            m_WeaponPool.Clear();
            for (int i = 0; i < m_ItemTypes.Count; ++i) {
                m_WeaponPool.Add(m_DeathmatchAgent.WeaponStatForItemType(m_ItemTypes[i]));
            }

            // Start eliminating possible weapons.
            var targetDistance = float.MaxValue;
            Transform targetParent = null;
            if (m_Target.Value != null) {
                targetParent = Utility.GetComponentForType<Animator>(m_Target.Value, true).transform;
                targetDistance = Vector3.Distance(transform.position, targetParent.position);
            }
            var containsGroupDamage = false;
            for (int i = m_WeaponPool.Count - 1; i > -1; --i) {
                // Can't use the weapon if it is out of ammo.
                if (m_Inventory.GetItemCount(m_WeaponPool[i].ItemType, true) + m_Inventory.GetItemCount(m_WeaponPool[i].ItemType, false) == 0) {
                    m_WeaponPool.RemoveAt(i);
                    continue;
                }

                // Can't use the weapon if it isn't within range.
                if (targetDistance != float.MaxValue) {
                    if (m_WeaponPool[i].GetUseLikelihood(targetDistance) <= 0) {
                        m_WeaponPool.RemoveAt(i);
                        continue;
                    }
                }

                // Don't use the grenade if:
                // - It has been thrown recently.
                // - A rocket has been fired recently.
                // - There is no target.
                // - The target isn't at the same relative height as the agent.
                if (m_WeaponPool[i].Class == WeaponClass.Grenade) {
                    if (m_GrenadeThrowTime + m_GrenadeTimeout.Value > Time.time || m_RocketFireTime + m_RocketTimeout.Value > Time.time  ||
                        targetDistance == float.MaxValue || Mathf.Abs(targetParent.position.y - transform.position.y) > 2) {
                        m_WeaponPool.RemoveAt(i);
                        continue;
                    }
                }

                // Don't use the rocket if:
                // - It has been fired recently.
                // - A grenade has been thrown recently.
                // - There is no target.
                if (m_WeaponPool[i].Class == WeaponClass.Power) {
                    if (m_RocketFireTime + m_RocketTimeout.Value > Time.time || m_GrenadeThrowTime + m_GrenadeTimeout.Value > Time.time || targetDistance == float.MaxValue) {
                        m_WeaponPool.RemoveAt(i);
                        continue;
                    }
                }

                // Don't check for group damage if no weapons can damage groups.
                if (!containsGroupDamage) {
                    containsGroupDamage = m_WeaponPool[i].GroupDamage;
                }
            }
            
            // Any of remaining weapons in the weapon pool can be used. Determine which weapon is best for the current situation.
            if (m_WeaponPool.Count == 1) {
                m_TargetItemType.Value = m_WeaponPool[0].ItemType;
                return TaskStatus.Success;
            }

            var itemIndex = -1;
            if (targetDistance != float.MaxValue) {
                // Use a power weapon if low on health and not too close to the target.
                if (m_CurrentHealth.Get() < m_LowHealth.Value) {
                    itemIndex = WeaponClassIndex(WeaponClass.Power);
                    if (itemIndex != -1 && m_WeaponPool[itemIndex].GetUseLikelihood(targetDistance) > 0) {
                        m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                        return TaskStatus.Success;
                    }
                }

                // Use a weapon which adds group damage if there are many enemies near the target. Use > because the current agent will be included in the group count.
                var maxLikelihood = float.MinValue;
                if (containsGroupDamage) {
#if UNITY_5_1 || UNITY_5_2
                    if ((m_HitColliders = Physics.OverlapSphere(targetParent.position, m_GroupRadius.Value, (1 << gameObject.layer) | (1 << m_DeathmatchAgent.TargetLayerMask))).Length > m_GroupCount.Value) {
#else
                    if (Physics.OverlapSphereNonAlloc(targetParent.position, m_GroupRadius.Value, m_HitColliders, (1 << gameObject.layer) | (1 << m_DeathmatchAgent.TargetLayerMask)) > m_GroupCount.Value) {
#endif
                        var containsTeammates = false;
                        // Do not use a group damage weapon if there are teammates nearby.
                        if (TeamManager.IsInstantiated) {
                            for (int i = 0; i < m_HitColliders.Length; ++i) {
                                if (m_HitColliders[i].gameObject == gameObject) {
                                    continue;
                                }

                                if (TeamManager.IsTeammate(gameObject, m_HitColliders[i].gameObject)) {
                                    containsTeammates = true;
                                    break;
                                }
                            }
                        }

                        if (!containsTeammates) {
                            for (int i = 0; i < m_WeaponPool.Count; ++i) {
                                if (m_WeaponPool[i].GroupDamage) {
                                    var likelihood = m_WeaponPool[i].GetUseLikelihood(targetDistance);
                                    if (likelihood > maxLikelihood) {
                                        maxLikelihood = likelihood;
                                        itemIndex = i;
                                    }
                                }
                            }

                            if (itemIndex != -1) {
                                m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                                return TaskStatus.Success;
                            }
                        }
                    }
                }

                // Loop through all of the available weapons. Determine if a particular weapon has a clear advantage for the particular distance.
                maxLikelihood = float.MinValue;
                var secondaryLikelihood = float.MinValue;
                for (int i = 0; i < m_WeaponPool.Count; ++i) {
                    var likelihood = m_WeaponPool[i].GetUseLikelihood(targetDistance);
                    if (likelihood > maxLikelihood) {
                        secondaryLikelihood = maxLikelihood;
                        maxLikelihood = likelihood;
                        itemIndex = i;
                    }
                }
                if (itemIndex != -1 && secondaryLikelihood != float.MinValue && maxLikelihood / secondaryLikelihood > m_LikelihoodAdvantage.Value) {
                    m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                    return TaskStatus.Success;
                }
            }

            // There isn't a clear advantage of using one weapon over another. Use the primary weapon if it is available, otherwise the secondary.
            itemIndex = WeaponClassIndex(WeaponClass.Primary);
            if (itemIndex != -1) {
                m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                return TaskStatus.Success;
            }

            itemIndex = WeaponClassIndex(WeaponClass.Secondary);
            if (itemIndex != -1) {
                m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                return TaskStatus.Success;
            }

            // There is no primary or secondary weapon available. Use the melee weapon.
            itemIndex = WeaponClassIndex(WeaponClass.Melee);
            if (itemIndex != -1) {
                m_TargetItemType.Value = m_WeaponPool[itemIndex].ItemType;
                return TaskStatus.Success;
            }

            // No items are within their range. If the target distance is too far then use the primary or secondary weapon (depending on which weapon has ammo).
            // If the target distance is too close or both weapons are out of ammo then then use the melee weapon.
            DeathmatchAgent.WeaponStat primaryWeapon = null;
            DeathmatchAgent.WeaponStat secondaryWeapon = null;
            DeathmatchAgent.WeaponStat meleeWeapon = null;
            DeathmatchAgent.WeaponStat weaponStat = null;
            for (int i = 0; i < m_ItemTypes.Count; ++i) {
                if ((weaponStat = m_DeathmatchAgent.WeaponStatForItemType(m_ItemTypes[i])).Class == WeaponClass.Primary) {
                    primaryWeapon = weaponStat;
                } else if ((weaponStat = m_DeathmatchAgent.WeaponStatForItemType(m_ItemTypes[i])).Class == WeaponClass.Secondary) {
                    secondaryWeapon = weaponStat;
                } else if ((weaponStat = m_DeathmatchAgent.WeaponStatForItemType(m_ItemTypes[i])).Class == WeaponClass.Melee) {
                    meleeWeapon = weaponStat;
                }
            }
            // Use the primary or secondary weapon if the target is far away.
            if (targetDistance > primaryWeapon.MaxUse) {
                if (m_Inventory.GetItemCount(primaryWeapon.ItemType, true) + m_Inventory.GetItemCount(primaryWeapon.ItemType, false) > 0) {
                    m_TargetItemType.Value = primaryWeapon.ItemType;
                    return TaskStatus.Success;
                }

                if (secondaryWeapon != null && (m_Inventory.GetItemCount(secondaryWeapon.ItemType, true) + m_Inventory.GetItemCount(secondaryWeapon.ItemType, false) > 0)) {
                    m_TargetItemType.Value = secondaryWeapon.ItemType;
                    return TaskStatus.Success;
                }
            }
            // Either the target distance is too close or the primary and secondary weapons are out of ammo. Return the melee weapon.
            if (meleeWeapon != null) {
                m_TargetItemType.Value = meleeWeapon.ItemType;
                return TaskStatus.Success;
            }

            // There are no weapons available. Return failure.
            return TaskStatus.Failure;
        }

        /// <summary>
        /// Returns the index of the specified WeaponClass.
        /// </summary>
        /// <param name="weaponClass">The WeaponClass to compare to.</param>
        /// <returns>The index of the specified WeaonClass. -1 if there are no weapons within the weapon pool which match the WeaponClass.</returns>
        private int WeaponClassIndex(WeaponClass weaponClass)
        {
            for (int i = 0; i < m_WeaponPool.Count; ++i) {
                if (m_WeaponPool[i].Class == weaponClass) {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// The grenade has been thrown. Remember the time to prevent the grenade from being thrown again too quickly.
        /// </summary>
        private void ThrewGrenade()
        {
            m_GrenadeThrowTime = Time.time;
        }

        /// <summary>
        /// The rocket has been fired. Remember the time to prevent the rocket from being fired again too quickly.
        /// </summary>
        private void FiredRocket()
        {
            m_RocketFireTime = Time.time;
        }
    }
}