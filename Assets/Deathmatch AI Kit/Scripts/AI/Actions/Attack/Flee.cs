﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Movement;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Flees in the opposite direction of the attacker.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class Flee : NavMeshMovement
    {
        [Tooltip("The agent has fleed when they are greater than the specified flee distance away from the attacker")]
        [SerializeField] protected SharedFloat m_FleeDistance = 10;

        // Component references
        private GameObject m_Attacker;

        /// <summary>
        /// Registers for any interested events.
        /// </summary>
        public override void OnAwake()
        {
            base.OnAwake();

            EventHandler.RegisterEvent<float, Vector3, Vector3, GameObject>(gameObject, "OnHealthDamageDetails", OnDamage);
        }

        /// <summary>
        /// Flees in the opposite direction of the attacker.
        /// </summary>
        /// <returns>Success after the agent has fled.</returns>
        public override TaskStatus OnUpdate()
        {
            if (m_Attacker == null) {
                return TaskStatus.Failure;
            }

            var direction = (transform.position - m_Attacker.transform.position);
            direction.y = 0;
            if (direction.magnitude > m_FleeDistance.Value) {
                return TaskStatus.Success;
            }

            // The attacker is still close. Keep fleeing.
            SetDestination(transform.position + direction.normalized * m_FleeDistance.Value);

            return TaskStatus.Running;
        }

        /// <summary>
        /// The agent has taken damage. Stop taking cover to avoid fire.
        /// </summary>
        /// <param name="amount">The amount of damage received.</param>
        /// <param name="position">The position of the damage.</param>
        /// <param name="force">The force of the damage.</param>
        /// <param name="attacker">The GameObject that attacked the agent.</param>
        private void OnDamage(float amount, Vector3 position, Vector3 force, GameObject attacker)
        {
            m_Attacker = attacker;
        }
    }
}