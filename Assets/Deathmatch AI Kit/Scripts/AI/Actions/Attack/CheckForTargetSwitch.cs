﻿using UnityEngine;
using Opsive.ThirdPersonController;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Determines if the agent should switch targets after attacked.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class CheckForTargetSwitch : Action
    {
        [Tooltip("A set of targets that the agent should ignore. This set will be added to when the agent switches to a new target to prevent the agent from switching back to the old")]
        [SerializeField] protected SharedGameObjectSet m_IgnoreTargets;
        [Tooltip("Switch targets if the target's health is less than this amount")]
        [SerializeField] protected SharedFloat m_SwitchHealth;
        [Tooltip("Switch targets if a random value is less than the switch probability")]
        [SerializeField] protected SharedFloat m_ForceSwitchProbability;
        [Tooltip("The target that the agent should attack")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The target that the agent may attack")]
        [SerializeField] protected SharedGameObject m_PossibleTarget;

        // Internal variables
        private Vector3 m_DamageForce;
        private GameObject m_Attacker;

        // Component references
        private DeathmatchAgent m_DeathmatchAgent;

        /// <summary>
        /// Cache the component references
        /// </summary>
        public override void OnAwake()
        {
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();

            EventHandler.RegisterEvent<float, Vector3, Vector3, GameObject>(gameObject, "OnHealthDamageDetails", OnDamage);
        }

        /// <summary>
        /// Determine if the target should be switched.
        /// </summary>
        /// <returns>Success if the target was switched.</returns>
        public override TaskStatus OnUpdate()
        {
            // Don't switch targets if there are no attackers.
            if (m_Attacker == null) {
                return TaskStatus.Failure;
            }
            var switchTargets = false;
            if (m_Target.Value == null || m_Attacker != Utility.GetComponentForType<Animator>(m_Target.Value, true).gameObject) {
                // Switch to the target attacking the agent if:
                // - The agent does not currently have any targets.
                // - The target's health value is lower than the switch health.
                // - A random probability less than the switch probability.
                if (m_Target.Value == null) {
                    switchTargets = true;
                } else if (!m_IgnoreTargets.Value.Contains(m_Attacker.transform.gameObject)) {
                    var currentTargetHealth = Utility.GetComponentForType<Health>(m_Target.Value, true);
                    if (currentTargetHealth.CurrentHealth > m_SwitchHealth.Value || Random.value < m_ForceSwitchProbability.Value) {
                        switchTargets = true;
                    }
                }
            }
            if (switchTargets) {
                // A new target was found, switch targets.
                m_PossibleTarget.Value = m_DeathmatchAgent.GetTargetBoneTransform(m_Attacker.transform).gameObject;
                return TaskStatus.Success;
            }
            return TaskStatus.Failure;
        }

        /// <summary>
        /// The task has ended.
        /// </summary>
        public override void OnEnd()
        {
            m_Attacker = null;
        }

        /// <summary>
        /// The agent has taken damage.
        /// </summary>
        /// <param name="amount">The amount of damage received.</param>
        /// <param name="position">The position of the damage.</param>
        /// <param name="force">The force of the damage.</param>
        /// <param name="attacker">The GameObject that attacked the agent.</param>
        private void OnDamage(float amount, Vector3 position, Vector3 force, GameObject attacker)
        {
            m_Attacker = attacker;
        }
    }
}