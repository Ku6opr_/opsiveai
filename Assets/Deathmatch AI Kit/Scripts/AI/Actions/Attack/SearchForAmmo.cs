﻿using UnityEngine;
#if !(UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4)
using UnityEngine.AI;
#endif
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Movement;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using System.Collections.Generic;
using Opsive.ThirdPersonController;
using BehaviorDesigner.Runtime.Tasks.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Actions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Searches for ammo.")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class SearchForAmmo : NavMeshMovement
    {
        [Tooltip("The ItemType which is low on ammo")]
        [SharedRequired] [SerializeField] protected SharedItemType m_ItemType;
        [Tooltip("Specifies the maximum distance that the character will search for an item.")]
        [SerializeField] protected SharedFloat m_MaxDistance = 20;

        // Internal variables
        private Dictionary<ItemType, List<Transform>> m_ItemPickups = new Dictionary<ItemType, List<Transform>>();
        private Vector3 m_TargetPosition;
        private NavMeshPath m_NavMeshPath = new NavMeshPath();
        private bool m_PathFound;

        /// <summary>
        /// Cache the component references and initialize the default values.
        /// </summary>
        public override void OnAwake()
        {
            base.OnAwake();

            // Cache all of the ItemPickup locations to prevent having to use FindObjects every frame.
            var allItemPickups = GameObject.FindObjectsOfType<ItemPickup>();
            for (int i = 0; i < allItemPickups.Length; ++i) {
                for (int j = 0; j < allItemPickups[i].ItemList.Count; ++j) {
                    // The PrimaryItemType will always contain ConsumableItemTypes that belong to that PrimaryItemType.
                    if (!(allItemPickups[i].ItemList[j].ItemType is PrimaryItemType)) {
                        continue;
                    }
                    List<Transform> itemPickups;
                    if (!m_ItemPickups.TryGetValue(allItemPickups[i].ItemList[j].ItemType, out itemPickups)) {
                        itemPickups = new List<Transform>();
                        m_ItemPickups.Add(allItemPickups[i].ItemList[j].ItemType, itemPickups);
                    }
                    itemPickups.Add(allItemPickups[i].transform);
                    break;
                }
            }
        }

        /// <summary>
        /// Determine the closest ammo position.
        /// </summary>
        public override void OnStart()
        {
            base.OnStart();

            List<Transform> itemPickups;
            m_PathFound = false;
            if (!m_ItemPickups.TryGetValue(m_ItemType.Value, out itemPickups)) {
                return;
            }

            // Move to the closest ItemPickup.
            var closestDistance = Mathf.Infinity;
            float distance;
            for (int i = 0; i < itemPickups.Count; ++i) {
                // Don't go for the item if it's not there.
                if (!itemPickups[i].gameObject.activeInHierarchy) {
                    continue;
                }
                // Use the NavMesh to determine the closest position - just because the item is physically the closest it doesn't mean that the path distance is the closest.
                NavMesh.CalculatePath(transform.position, itemPickups[i].position, NavMesh.AllAreas, m_NavMeshPath);
                if (m_NavMeshPath.corners.Length > 0) {
                    distance = 0;
                    var prevCorner = m_NavMeshPath.corners[0];
                    for (int j = 1; j < m_NavMeshPath.corners.Length; ++j) {
                        distance += Vector3.Distance(m_NavMeshPath.corners[j], prevCorner);
                        prevCorner = m_NavMeshPath.corners[j];
                        // Stop determining the distance if too far away.
                        if (distance > m_MaxDistance.Value) {
                            distance = float.MaxValue;
                            break;
                        }
                    }
                    // Go to the position that has the least distance.
                    if (distance < closestDistance) {
                        closestDistance = distance;
                        m_TargetPosition = itemPickups[i].position;
                    }
                    m_PathFound = true;
                }
            }

            if (m_PathFound) {
                SetDestination(m_TargetPosition);
            }
        }

        /// <summary>
        /// Return Success when the agent has arrived at the ammo position.
        /// </summary>
        /// <returns>Success when the agent has arrived at the ammo position, otherwise Running.</returns>
        public override TaskStatus OnUpdate()
        {
            if (!m_PathFound) {
                return TaskStatus.Failure;
            }
            if (HasArrived()) {
                return TaskStatus.Success;
            }
            return TaskStatus.Running;
        }
    }
}