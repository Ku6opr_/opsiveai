using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Conditions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Should the agent find cover?")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class ShouldFindCover : Conditional
    {
        [Tooltip("The target to attack")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The current cover point")]
        [SerializeField] protected SharedCoverPoint m_CoverPoint;

        // Internal variables
        private CoverPoint[] m_CoverPoints;

        /// <summary>
        /// Initialize the default values.
        /// </summary>
        public override void OnAwake()
        {
            base.OnAwake();

            m_CoverPoints = GameObject.FindObjectsOfType<CoverPoint>();
        }

        /// <summary>
        /// Return Success if the agent should find cover.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
            // The agent could already be in cover if they were camping in a cover point.
            if (m_CoverPoint.Value != null) {
                return TaskStatus.Success;
            }
            
            // Is there a valid cover position? Return success as soon as there is a valid position.
            for (int i = 0; i < m_CoverPoints.Length; ++i) {
                if (m_CoverPoints[i].IsValidCoverPoint(transform, m_Target.Value == null ? null : m_Target.Value.transform)) {
                    m_CoverPoint.Value = m_CoverPoints[i];
                    m_CoverPoint.Value.Occupant = transform;
                    return TaskStatus.Success;
                }
            }

            return TaskStatus.Failure;
        }
    }
}