using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;
using Opsive.ThirdPersonController;
using BehaviorDesigner.Runtime.Tasks.ThirdPersonController;

namespace Opsive.DeathmatchAIKit.AI.Conditions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Is the agent low on primary or secondary ammo?")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class IsAmmoLow : Conditional
    {
        [Tooltip("The agent is considered to have low ammo when the count less than the specified value")]
        [SerializeField] protected SharedInt m_Amount = 5;
        [Tooltip("Should the agent consider if any power weapons are considered low?")]
        [SerializeField] protected SharedBool m_PowerWeapon;
        [Tooltip("Should the total ammo be considered instead of the individual item's ammo?")]
        [SerializeField] protected SharedBool m_TotalAmmo;
        [Tooltip("The ItemType which is low on ammo")]
        [SharedRequired] [SerializeField] protected SharedItemType m_LowAmmoItemType;

        // Internal variables
        private ItemType m_PrimaryItemType;
        private ItemType m_SecondaryItemType;
        private ItemType m_PowerItemType;

        // Component references
        private Inventory m_Inventory;

        /// <summary>
        /// Cache the component references and initialize the default values.
        /// </summary>
        public override void OnAwake()
        {
            m_Inventory = GetComponent<Inventory>();

            // Loop through the available weapons to find the correct weapon classes.
            var deathmatchAgent = GetComponent<DeathmatchAgent>();
            for (int i = 0; i < deathmatchAgent.AvailableWeapons.Length; ++i) {
                if (deathmatchAgent.AvailableWeapons[i].Class == DeathmatchAgent.WeaponStat.WeaponClass.Primary) {
                    m_PrimaryItemType = deathmatchAgent.AvailableWeapons[i].ItemType;
                } else if (deathmatchAgent.AvailableWeapons[i].Class == DeathmatchAgent.WeaponStat.WeaponClass.Secondary) {
                    m_SecondaryItemType = deathmatchAgent.AvailableWeapons[i].ItemType;
                } else if (deathmatchAgent.AvailableWeapons[i].Class == DeathmatchAgent.WeaponStat.WeaponClass.Power) {
                    m_PowerItemType = deathmatchAgent.AvailableWeapons[i].ItemType;
                }
            }
        }

        /// <summary>
        /// Returns Success if the agent is low on ammo.
        /// </summary>
        /// <returns>Success if the agent is low on ammo.</returns>
        public override TaskStatus OnUpdate()
        {
            // Power weapons are treated indepndently of the primary and secondary weapons.
            if (m_PowerWeapon.Value) {
                if (m_PowerItemType != null && IsItemAmmoLow(m_PowerItemType)) {
                    m_LowAmmoItemType.Value = m_PowerItemType;
                    return TaskStatus.Success;
                }
            } else {
                // If TotalAmmo is true then it doesn't matter which item has ammo as long as at least one item does have ammo.
                if (m_TotalAmmo.Value) {
                    if (m_PrimaryItemType != null && m_SecondaryItemType != null && IsItemAmmoLow(m_PrimaryItemType) && IsItemAmmoLow(m_SecondaryItemType)) {
                        m_LowAmmoItemType.Value = m_PrimaryItemType;
                        return TaskStatus.Success;
                    }
                } else {
                    if (m_PrimaryItemType != null && IsItemAmmoLow(m_PrimaryItemType)) {
                        m_LowAmmoItemType.Value = m_PrimaryItemType;
                        return TaskStatus.Success;
                    }

                    if (m_SecondaryItemType != null  && IsItemAmmoLow(m_SecondaryItemType)) {
                        m_LowAmmoItemType.Value = m_SecondaryItemType;
                        return TaskStatus.Success;
                    }
                }
            }

            return TaskStatus.Failure;
        }

        /// <summary>
        /// Returns true if the specified ItemType's ammo is low.
        /// </summary>
        /// <param name="itemType">The ItemType to check if low.</param>
        /// <returns>True if the specified ItemType's ammo is low.</returns>
        private bool IsItemAmmoLow(ItemType itemType)
        {
            return (m_Inventory.GetItemCount(itemType, true) + m_Inventory.GetItemCount(itemType, false)) <= m_Amount.Value;
        }
    }
}