using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Opsive.ThirdPersonController;
using Tooltip = BehaviorDesigner.Runtime.Tasks.TooltipAttribute;

namespace Opsive.DeathmatchAIKit.AI.Conditions
{
    [TaskCategory("Deathmatch AI Kit")]
    [TaskDescription("Is the target in sight?")]
    [TaskIcon("Assets/Deathmatch AI Kit/Editor/Images/Icons/DeathmatchAIKitIcon.png")]
    public class IsTargetInSight : Conditional
    {
        [Tooltip("The existing target GameObject")]
        [SerializeField] protected SharedGameObject m_Target;
        [Tooltip("The maximum field of view that the agent can see other targets (in degrees)")]
        [SerializeField] protected SharedFloat m_FieldOfView = 90;
        [Tooltip("The maximum distance that the agent can see other targets")]
        [SerializeField] protected SharedFloat m_MaxTargetDistance;
        [Tooltip("The GameObject within sight")]
        [SharedRequired] [SerializeField] protected SharedGameObject m_FoundTarget;
        [Tooltip("A set of targets that the agent should ignore")]
        [SharedRequired] [SerializeField] protected SharedGameObjectSet m_IgnoreTargets;

        // Component references
        private DeathmatchAgent m_DeathmatchAgent;

        /// <summary>
        /// Cache the component references.
        /// </summary>
        public override void OnAwake()
        {
            m_DeathmatchAgent = GetComponent<DeathmatchAgent>();
        }

        /// <summary>
        /// Returns success if the target is in sight and is alive.
        /// </summary>
        /// <returns>Success if the target is within sight and is alive.</returns>
        public override TaskStatus OnUpdate()
        {
            var target = m_DeathmatchAgent.TargetInSight(m_FieldOfView.Value, m_MaxTargetDistance.Value, m_IgnoreTargets.Value);
            if (target != null && (m_Target.Value == null || target != Utility.GetComponentForType<Animator>(m_Target.Value, true))) {
                var targetHealth = Utility.GetComponentForType<Health>(target);
                if (targetHealth.CurrentHealth > 0) {
                    m_FoundTarget.Value = m_DeathmatchAgent.GetTargetBoneTransform(target.transform).gameObject;
                }
            }
            return target != null ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}
