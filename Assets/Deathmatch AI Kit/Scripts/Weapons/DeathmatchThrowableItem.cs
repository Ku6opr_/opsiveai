﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Subclasses ThrowableItem and provides the necessary hooks for the Deathmatch AI Kit.
    /// </summary>
    public class DeathmatchThrowableItem : ThrowableItem
    {
        // Exposed properties
        public Vector3 ThrowForce { get { return m_ThrowForce; } set { m_ThrowForce = value; } }

        /// <summary>
        /// Throw the object.
        /// </summary>
        public override void Used()
        {
            base.Used();

            // The AI agent needs to be aware of when the grenade was thrown.
            EventHandler.ExecuteEvent(m_Character, "OnThrowItem");
        }
    }
}