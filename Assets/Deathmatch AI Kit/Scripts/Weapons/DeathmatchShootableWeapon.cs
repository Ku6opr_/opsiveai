﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// Subclasses ShootableWeapon and provides the necessary hooks for the Deathmatch AI Kit.
    /// </summary>
    public class DeathmatchShootableWeapon : ShootableWeapon
    {
        // Exposed properties
        public float Spread { set { m_Spread = value; } }

        protected override void ProjectileFire()
        {
            base.ProjectileFire();

            // The AI agent needs to be aware of when the projectile was fired.
            EventHandler.ExecuteEvent(m_Character, "OnFireProjectile");
        }
    }
}