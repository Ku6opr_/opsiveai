﻿using UnityEngine;
using Opsive.ThirdPersonController;

namespace Opsive.DeathmatchAIKit
{
    /// <summary>
    /// The ItemPickupLocator allows AI agents to detect items if they enter the locator's trigger.
    /// </summary>
    public class ItemPickupLocator : MonoBehaviour
    {
        // Component references
        private ItemPickup m_ParentItemPickup;

        // Exposed properties
        public ItemPickup ParentItemPickup { get { return m_ParentItemPickup; } }

        /// <summary>
        /// Cache the component references.
        /// </summary>
        private void Start()
        {
            m_ParentItemPickup = GetComponentInParent<ItemPickup>();
        }
    }
}