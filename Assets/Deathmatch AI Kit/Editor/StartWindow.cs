﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// A window which will launch the different builders and documentation links.
    /// </summary>
    [InitializeOnLoad]
    public class StartWindow : EditorWindow
    {
        private static string s_Version = "1.0";

        private GUIStyle m_TitleTextGUIStyle;
        private GUIStyle m_HeaderTextGUIStyle;
        private GUIStyle m_UpperTextGUIStyle;
        private GUIStyle m_LowerTextGUIStyle;
        private GUIStyle m_VersionTextGUIStyle;
        private GUIStyle m_VersionAvailableTextGUIStyle;

        private Texture2D m_HeaderTexture;
        private Texture2D m_HeaderIconTexture;
        private Texture2D m_AgentBuilderTexture;
        private Texture2D m_InstallGuideTexture;
        private Texture2D m_DocumentationTexture;
        private Texture2D m_VideosTexture;
        private Texture2D m_ForumTexture;
        private Texture2D m_ContactTexture;

        private Texture2D m_SeparatorTexture;

        private Rect m_TitleTextRect = new Rect(0, 10, 350, 30);
        private Rect m_AssetTextRect = new Rect(5, 48, 200, 20);
        private Rect m_ResourcesTextRect = new Rect(5, 179, 200, 20);

        private Rect m_TitleSeparatorRect = new Rect(0, 43, 350, 1);
        private Rect m_AssetSeparatorRect = new Rect(0, 174, 350, 1);
        private Rect m_ResourcesSeparatorRect = new Rect(0, 372, 350, 1);

        private Rect m_HeaderTextureRect = new Rect(0, 0, 350, 42);
        private Rect m_HeaderIconTextureRect = new Rect(0, 0, 42, 42);
        private Rect m_AgentBuilderTextureRect = new Rect(63, 69, 80, 80);
        private Rect m_InstallGuideTextureRect = new Rect(203, 69, 80, 80);
        private Rect m_DocumentationTextureRect = new Rect(76, 202, 60, 60);
        private Rect m_VideosTextureRect = new Rect(214, 202, 60, 60);
        private Rect m_ForumTextureRect = new Rect(76, 287, 60, 60);
        private Rect m_ContactTextureRect = new Rect(214, 287, 60, 60);

        private Rect m_AgentBuilderTextRect = new Rect(33, 149, 140, 40);
        private Rect m_InstallGuideTextRect = new Rect(173, 149, 140, 40);
        private Rect m_DocumentationTextRect = new Rect(51, 262, 110, 20);
        private Rect m_VideosTextRect = new Rect(189, 262, 110, 20);
        private Rect m_ForumTextRect = new Rect(51, 347, 110, 20);
        private Rect m_ContactTextRect = new Rect(189, 347, 110, 20);
        
        private Rect m_VersionTextRect = new Rect(5, 376, 110, 20);

        /// <summary>
        /// Perform editor checks as soon as the scripts are done compiling.
        /// </summary>
        static StartWindow()
        {
            EditorApplication.update += EditorStartup;
        }

        [MenuItem("Tools/Deathmatch AI Kit/Start Window", false, 0)]
        public static void ShowWindow()
        {
            var window = EditorWindow.GetWindow<StartWindow>(true, "Deathmatch AI Kit Start Window");
            window.minSize = window.maxSize = new Vector2(350, 395);
        }

        /// <summary>
        /// Show the StartWindow if it hasn't been shown before.
        /// </summary>
        private static void EditorStartup()
        {
            if (!EditorApplication.isCompiling) {
                if (!EditorPrefs.GetBool("Opsive.DeathmatchAIKit.StartWindowShown", false)) {
                    EditorPrefs.SetBool("Opsive.DeathmatchAIKit.StartWindowShown", true);
                    ShowWindow();
                }

                EditorApplication.update -= EditorStartup;
            }
        }

        /// <summary>
        /// Show the textures and text, along with launching the clicked topic.
        /// </summary>
        private void OnGUI()
        {
            Initialize();

            // Draw the title and header text.
            GUI.DrawTexture(m_HeaderTextureRect, m_HeaderTexture);
            GUI.DrawTexture(m_HeaderIconTextureRect, m_HeaderIconTexture);
            GUI.Label(m_TitleTextRect, "Deathmatch AI Kit", m_TitleTextGUIStyle);
            GUI.DrawTexture(m_TitleSeparatorRect, m_SeparatorTexture);
            GUI.Label(m_AssetTextRect, "Asset", m_HeaderTextGUIStyle);
            GUI.DrawTexture(m_AssetSeparatorRect, m_SeparatorTexture);
            GUI.Label(m_ResourcesTextRect, "Resources", m_HeaderTextGUIStyle);
            GUI.DrawTexture(m_ResourcesSeparatorRect, m_SeparatorTexture);

            // Draw the textures.
            GUI.DrawTexture(m_AgentBuilderTextureRect, m_AgentBuilderTexture);
            GUI.DrawTexture(m_InstallGuideTextureRect, m_InstallGuideTexture);
            GUI.DrawTexture(m_DocumentationTextureRect, m_DocumentationTexture);
            GUI.DrawTexture(m_VideosTextureRect, m_VideosTexture);
            GUI.DrawTexture(m_ForumTextureRect, m_ForumTexture);
            GUI.DrawTexture(m_ContactTextureRect, m_ContactTexture);

            // Draw the text.
            GUI.Label(m_AgentBuilderTextRect, "Agent Builder", m_UpperTextGUIStyle);
            GUI.Label(m_InstallGuideTextRect, "Install Guide", m_LowerTextGUIStyle);
            GUI.Label(m_DocumentationTextRect, "Documentation", m_LowerTextGUIStyle);
            GUI.Label(m_VideosTextRect, "Videos", m_LowerTextGUIStyle);
            GUI.Label(m_ForumTextRect, "Forum", m_LowerTextGUIStyle);
            GUI.Label(m_ContactTextRect, "Contact", m_LowerTextGUIStyle);
            
            GUI.Label(m_VersionTextRect, "Version " + s_Version, m_VersionTextGUIStyle);

            // Change to the correct cursor type when the user is hovering over a link.
            EditorGUIUtility.AddCursorRect(m_AgentBuilderTextureRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_InstallGuideTextureRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_DocumentationTextureRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_VideosTextureRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_ForumTextureRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_ContactTextureRect, MouseCursor.Link);

            EditorGUIUtility.AddCursorRect(m_AgentBuilderTextRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_DocumentationTextRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_VideosTextRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_ForumTextRect, MouseCursor.Link);
            EditorGUIUtility.AddCursorRect(m_ContactTextRect, MouseCursor.Link);

            // Open the window/link on a click.
            if (Event.current.type == EventType.MouseUp) {
                var mousePosition = Event.current.mousePosition;
                if (m_AgentBuilderTextureRect.Contains(mousePosition) || m_AgentBuilderTextRect.Contains(mousePosition)) {
                    AgentBuilder.ShowWindow();
                } else if (m_InstallGuideTextureRect.Contains(mousePosition) || m_InstallGuideTextRect.Contains(mousePosition)) {
                    Application.OpenURL("http://www.opsive.com/assets/DeathmatchAIKit/documentation.php?id=16");
                } else if (m_DocumentationTextureRect.Contains(mousePosition) || m_DocumentationTextRect.Contains(mousePosition)) {
                    Application.OpenURL("http://www.opsive.com/assets/DeathmatchAIKit/documentation.php");
                } else if (m_VideosTextureRect.Contains(mousePosition) || m_VideosTextRect.Contains(mousePosition)) {
                    Application.OpenURL("http://www.opsive.com/assets/DeathmatchAIKit/videos.php");
                } else if (m_ForumTextureRect.Contains(mousePosition) || m_ForumTextRect.Contains(mousePosition)) {
                    Application.OpenURL("http://www.opsive.com/forum");
                } else if (m_ContactTextureRect.Contains(mousePosition) || m_ContactTextRect.Contains(mousePosition)) {
                    Application.OpenURL("http://www.opsive.com/assets/DeathmatchAIKit/documentation.php?id=15");
                }
            }
        }

        /// <summary>
        /// Initialize the GUI textures and styles.
        /// </summary>
        private void Initialize()
        {
            if (m_HeaderTexture == null) {
                var editorPath = System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(this)));
                m_HeaderTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/StartWindowHeader.png", typeof(Texture2D)) as Texture2D;
                m_HeaderIconTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/DeathmatchAIKitIcon.png", typeof(Texture2D)) as Texture2D;
                m_AgentBuilderTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/AgentBuilder.png", typeof(Texture2D)) as Texture2D;
                m_InstallGuideTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/InstallGuide.png", typeof(Texture2D)) as Texture2D;
                m_DocumentationTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/Documentation.png", typeof(Texture2D)) as Texture2D;
                m_VideosTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/Videos.png", typeof(Texture2D)) as Texture2D;
                m_ForumTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/Forum.png", typeof(Texture2D)) as Texture2D;
                m_ContactTexture = AssetDatabase.LoadAssetAtPath(editorPath + "/Images/Icons/Contact.png", typeof(Texture2D)) as Texture2D;
            }

            if (m_TitleTextGUIStyle == null) {
                m_TitleTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_TitleTextGUIStyle.alignment = TextAnchor.UpperCenter;
                m_TitleTextGUIStyle.fontSize = 20;
                m_TitleTextGUIStyle.fontStyle = FontStyle.Bold;
                m_TitleTextGUIStyle.normal.textColor = new Color(0.706f, 0.706f, 0.706f, 1f);
            }

            if (m_HeaderTextGUIStyle == null) {
                m_HeaderTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_HeaderTextGUIStyle.alignment = TextAnchor.UpperLeft;
                m_HeaderTextGUIStyle.fontSize = 15;
                m_HeaderTextGUIStyle.fontStyle = FontStyle.Bold;
            }

            if (m_UpperTextGUIStyle == null) {
                m_UpperTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_UpperTextGUIStyle.alignment = TextAnchor.UpperCenter;
                m_UpperTextGUIStyle.fontSize = 14;
                m_UpperTextGUIStyle.fontStyle = FontStyle.Bold;
            }

            if (m_LowerTextGUIStyle == null) {
                m_LowerTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_LowerTextGUIStyle.alignment = TextAnchor.UpperCenter;
                m_LowerTextGUIStyle.fontSize = 12;
                m_LowerTextGUIStyle.fontStyle = FontStyle.Bold;
            }

            if (m_VersionTextGUIStyle == null) {
                m_VersionTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_VersionTextGUIStyle.alignment = TextAnchor.UpperLeft;
            }

            if (m_VersionAvailableTextGUIStyle == null) {
                m_VersionAvailableTextGUIStyle = new GUIStyle(GUI.skin.label);
                m_VersionAvailableTextGUIStyle.alignment = TextAnchor.UpperLeft;
                m_VersionAvailableTextGUIStyle.normal.textColor = Color.green;
            }

            if (m_SeparatorTexture == null) {
                m_SeparatorTexture = new Texture2D(1, 1);
                m_SeparatorTexture.SetPixel(0, 0, m_HeaderTextGUIStyle.normal.textColor);
            }
        }
    }
}