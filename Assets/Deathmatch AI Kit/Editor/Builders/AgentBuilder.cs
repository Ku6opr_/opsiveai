﻿using UnityEngine;
using UnityEditor;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Abilities;
using Opsive.DeathmatchAIKit.AI;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Editor;
using System;
using System.IO;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// Shows a wizard that will build a new Deathmatch AI Kit character.
    /// </summary>
    public class AgentBuilder : EditorWindow
    {
        // Window variables
        private GUIStyle m_HeaderLabelStyle;

        // Agent variables
        private GameObject m_Agent;
        private bool m_AddToTeam;
        private Transform[] m_Waypoints;

        // Exposed variables
        public GameObject Agent { set { m_Agent = value; } }

        [MenuItem("Tools/Deathmatch AI Kit/Agent Builder", false, 11)]
        public static void ShowWindow()
        {
            var window = EditorWindow.GetWindow<AgentBuilder>(true, "Agent Builder");
            window.minSize = window.maxSize = new Vector2(350, 160);
        }

        /// <summary>
        /// Initializes the GUIStyle used by the header.
        /// </summary>
        private void OnEnable()
        {
            if (m_HeaderLabelStyle == null) {
                m_HeaderLabelStyle = new GUIStyle(EditorStyles.label);
                m_HeaderLabelStyle.wordWrap = true;
            }
        }

        /// <summary>
        /// Shows the Agent Builder.
        /// </summary>
        private void OnGUI()
        {
            var description = "This wizard will assist in the creation of a new agent.";
            EditorGUILayout.LabelField(description, m_HeaderLabelStyle);
            GUILayout.Space(5);

            var canBuild = ShowGUI();

            GUILayout.Space(5);
            GUI.enabled = canBuild;
            if (GUILayout.Button("Build")) {
                BuildAgent();
            }
        }

        /// <summary>
        /// Shows the Agent Builder options.
        /// </summary>
        private bool ShowGUI()
        {
            var canContinue = true;
            m_Agent = EditorGUILayout.ObjectField("Character", m_Agent, typeof(GameObject), true) as GameObject;
            if (m_Agent == null) {
                EditorGUILayout.HelpBox("Select the GameObject which will be used as the agent. You must first run the Character Builder on the agent.",
                                    MessageType.Error);
                canContinue = false;
            } else if (PrefabUtility.GetPrefabType(m_Agent) == PrefabType.Prefab) {
                EditorGUILayout.HelpBox("Please drag your character into the scene. The Agent Builder cannot add components to prefabs.",
                                    MessageType.Error);
                canContinue = false;
            } else if (m_Agent.GetComponent<RigidbodyCharacterController>() == null) {
                EditorGUILayout.HelpBox("This agent must first be created with the Character Builder.",
                                    MessageType.Error);
                if (GUILayout.Button("Open Character Builder")) {
                    ThirdPersonController.Editor.CharacterBuilder.ShowWindow();
                    var windows = Resources.FindObjectsOfTypeAll(typeof(Opsive.ThirdPersonController.Editor.CharacterBuilder));
                    if (windows != null && windows.Length > 0) {
                        var characterBuilder = windows[0] as ThirdPersonController.Editor.CharacterBuilder;
                        characterBuilder.Character = m_Agent;
                        characterBuilder.AIAgent = true;
                    }
                }
                canContinue = false;
            }

            m_AddToTeam = EditorGUILayout.Toggle("Add to Team", m_AddToTeam);

            return canContinue;
        }

        /// <summary>
        /// Builds the agent.
        /// </summary>
        private void BuildAgent()
        {
            // Add the Deathmatch Agent component.
            var deathmatchAgent = m_Agent.AddComponent<DeathmatchAgent>();
            if (m_AddToTeam) {
                deathmatchAgent.AddToTeam = true;
            }

            // The Deathmatch Agent needs to know where to look.
            var lookTransform = new GameObject("Look Position").transform;
            lookTransform.parent = deathmatchAgent.transform;
            lookTransform.localPosition = new Vector3(0.3f, 1.6f, 0.35f);
            deathmatchAgent.LookTransform = lookTransform;

            // Add any existing items to the Weapon List.
            var items = m_Agent.GetComponentsInChildren<Item>();
            deathmatchAgent.AvailableWeapons = new DeathmatchAgent.WeaponStat[items.Length];
            for (int i = 0; i < items.Length; ++i) {
                var weaponStat = new DeathmatchAgent.WeaponStat();
                weaponStat.ItemType = items[i].ItemType;
                if (typeof(MeleeWeapon).IsAssignableFrom(items[i].GetType())) {
                    weaponStat.Class = DeathmatchAgent.WeaponStat.WeaponClass.Melee;
                } else if (typeof(ThrowableItem).IsAssignableFrom(items[i].GetType())) {
                    weaponStat.Class = DeathmatchAgent.WeaponStat.WeaponClass.Grenade;
                }
                deathmatchAgent.AvailableWeapons[i] = weaponStat;
            }
            DeathmatchAgentInspector.UpdateItemClasses(deathmatchAgent);

            // Add the Cover ability.
            if (m_Agent.GetComponent<Cover>() == null) {
                var cover = m_Agent.AddComponent<Cover>();
                cover.hideFlags = HideFlags.HideInInspector;
                cover.StartType = Ability.AbilityStartType.Manual;
                cover.StopType = Ability.AbilityStopType.Manual;

                var controller = m_Agent.GetComponent<RigidbodyCharacterController>();
                var abilities = controller.Abilities;
                cover.Index = abilities.Length;
                Array.Resize(ref abilities, abilities.Length + 1);
                abilities[abilities.Length - 1] = cover;
                controller.Abilities = abilities;
            }

            // Add the behavior tree component.
            var behaviorTree = m_Agent.AddComponent<BehaviorTree>();
            var baseDirectory = Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(this))).Replace("Editor/Builders", "");
            behaviorTree.ExternalBehavior = AssetDatabase.LoadAssetAtPath(baseDirectory + "Behavior Trees/" + (m_AddToTeam ? "Team.asset" : "Solo.asset"), typeof(ExternalBehaviorTree)) as ExternalBehaviorTree;
            var coverVariable = behaviorTree.GetVariable("Cover") as SharedCoverPoint;
            coverVariable.PropertyMapping = "Opsive.DeathmatchAIKit.AI.DeathmatchAgent/CoverPoint";
            coverVariable.PropertyMappingOwner = m_Agent;
            // Save the cover point variable mapping.
            if (BehaviorDesignerPreferences.GetBool(BDPreferences.BinarySerialization)) {
                BinarySerialization.Save(behaviorTree.GetBehaviorSource());
            } else {
                JSONSerialization.Save(behaviorTree.GetBehaviorSource());
            }

            // Add the NavMeshAgent and NavMeshAgentBridge component.
            m_Agent.AddComponent<UnityEngine.AI.NavMeshAgent>();
            m_Agent.AddComponent<NavMeshAgentBridge>();

            // The animator should always update and all of the SkinnedMeshRenderers should update to keep track of the agent's position.
            var animator = m_Agent.GetComponent<Animator>();
            animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            var skinnedMeshRenderers = m_Agent.GetComponentsInChildren<SkinnedMeshRenderer>();
            for (int i = 0; i < skinnedMeshRenderers.Length; ++i) {
                skinnedMeshRenderers[i].updateWhenOffscreen = true;
            }

            Selection.activeGameObject = m_Agent;

            Close();
        }
    }
}
