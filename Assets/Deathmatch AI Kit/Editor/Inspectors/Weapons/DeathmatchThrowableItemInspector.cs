using UnityEditor;
using Opsive.ThirdPersonController.Editor;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// Shows a custom inspector for DeathmatchThrowableItem.
    /// </summary>
    [CustomEditor(typeof(DeathmatchThrowableItem))]
    public class DeathmatchThrowableItemInspector : ThrowableItemInspector
    {
        // Intentionally left blank. The parent class contains all of the implementation.
    }
}