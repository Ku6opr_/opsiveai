using UnityEditor;
using Opsive.ThirdPersonController.Editor;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// Shows a custom inspector for DeathmatchShootableWeapon.
    /// </summary>
    [CustomEditor(typeof(DeathmatchShootableWeapon))]
    public class DeathmatchShootableWeaponInspector : ShootableWeaponInspector
    {
        // Intentionally left blank. The parent class contains all of the implementation.
    }
}