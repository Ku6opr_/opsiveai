using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Editor;
using Opsive.DeathmatchAIKit.AI;

namespace Opsive.DeathmatchAIKit.Editor
{
    /// <summary>
    /// Shows a custom inspector for DeathmatchAgent.
    /// </summary>
    [CustomEditor(typeof(DeathmatchAgent))]
    public class DeathmatchAgentInspector : InspectorBase
    {

        // DeathmatchAgent
        private DeathmatchAgent m_DeathmatchAgent;
        private SerializedProperty m_AvailableWeapons;
        private ReorderableList m_ReorderableWeaponList;

        /// <summary>
        /// Draws the custom inspector.
        /// </summary>
        public override void OnInspectorGUI()
        {
            m_DeathmatchAgent = target as DeathmatchAgent;
            if (m_DeathmatchAgent == null || serializedObject == null)
                return; // How'd this happen?

            base.OnInspectorGUI();

            // Show all of the fields.
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            if (Application.isPlaying) {
                GUI.enabled = false;
                EditorGUILayout.ObjectField("Cover Point", m_DeathmatchAgent.CoverPoint, typeof(GameObject), true);
                GUI.enabled = true;
            } else {
                // Ensure all of the items are using the correct item class.
                var items = m_DeathmatchAgent.GetComponentsInChildren<Item>();
                var incorrectItemClass = false;
                for (int i = 0; i < items.Length; ++i) {
                    if (items[i].GetType().Equals(typeof(ShootableWeapon)) || items[i].GetType().Equals(typeof(ThirdPersonController.Wrappers.ShootableWeapon)) ||
                        items[i].GetType().Equals(typeof(ThrowableItem)) || items[i].GetType().Equals(typeof(ThirdPersonController.Wrappers.ThrowableItem))) {
                        incorrectItemClass = true;
                    }
                }
                if (incorrectItemClass) {
                    EditorGUILayout.HelpBox("An item is not using the correct Deathmatch component. Do you want to update the items?", MessageType.Error);
                    if (GUILayout.Button("Update")) {
                        UpdateItemClasses(m_DeathmatchAgent);
                    }
                    GUILayout.Space(5);
                }
            }
            var addToTeamProperty = PropertyFromName(serializedObject, "m_AddToTeam");
            EditorGUILayout.PropertyField(addToTeamProperty);
            if (addToTeamProperty.boolValue) {
                EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_TeamIndex"));
            }
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_TargetLayerMask"));
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_IgnoreLayerMask"));
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_LookTransform"));
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_TargetBone"));
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_DistanceScoreWeight"));
            EditorGUILayout.PropertyField(PropertyFromName(serializedObject, "m_AngleScoreWeight"));

            if (m_ReorderableWeaponList == null) {
                m_AvailableWeapons = PropertyFromName(serializedObject, "m_AvailableWeapons");
                m_ReorderableWeaponList = new ReorderableList(serializedObject, m_AvailableWeapons, false, true, true, true);
                m_ReorderableWeaponList.drawHeaderCallback = (Rect rect) =>
                {
                    EditorGUI.LabelField(rect, "Available Weapons");
                };
                m_ReorderableWeaponList.drawElementCallback = OnAvailableWeaponListDraw;
                m_ReorderableWeaponList.onAddCallback = OnAvailableWeaponListAdd;
                m_ReorderableWeaponList.onRemoveCallback = OnAvailableWeaponListRemove;
                m_ReorderableWeaponList.onSelectCallback = (ReorderableList list) =>
                {
                    m_DeathmatchAgent.SelectedAvailableWeapon = list.index;
                };
                if (m_DeathmatchAgent.SelectedAvailableWeapon != -1) {
                    m_ReorderableWeaponList.index = m_DeathmatchAgent.SelectedAvailableWeapon;
                }
            }
            m_ReorderableWeaponList.DoLayoutList();
            // Draw the selected weapon.
            if (m_ReorderableWeaponList.index != -1) {
                if (m_ReorderableWeaponList.index < m_AvailableWeapons.arraySize) {
                    GUILayout.Space(4);
                    var weaponStatProperty = m_AvailableWeapons.GetArrayElementAtIndex(m_ReorderableWeaponList.index);
                    var itemTypeProperty = weaponStatProperty.FindPropertyRelative("m_ItemType");
                    var name = "(none)";
                    if (itemTypeProperty.objectReferenceValue is ItemType) {
                        name = (itemTypeProperty.objectReferenceValue as ItemType).name;
                    }
                    EditorGUILayout.LabelField(name);
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_ItemType"));
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_Class"));
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_UseLikelihood"));
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_MinUse"));
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_MaxUse"));
                    EditorGUILayout.PropertyField(weaponStatProperty.FindPropertyRelative("m_GroupDamage"));
                } else {
                    m_ReorderableWeaponList.index = m_DeathmatchAgent.SelectedAvailableWeapon = -1;
                }
            }

            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(m_DeathmatchAgent, "Inspector");
                serializedObject.ApplyModifiedProperties();
                InspectorUtility.SetObjectDirty(m_DeathmatchAgent);
            }
        }

        /// <summary>
        /// Draws all of the added available weapons.
        /// </summary>
        private void OnAvailableWeaponListDraw(Rect rect, int index, bool isActive, bool isFocused)
        {
            EditorGUI.BeginChangeCheck();
            var weaponStatProperty = m_AvailableWeapons.GetArrayElementAtIndex(index);
            var itemTypeProperty = weaponStatProperty.FindPropertyRelative("m_ItemType");
            var name = "(none)";
            if (itemTypeProperty.objectReferenceValue is ItemType) {
                name = (itemTypeProperty.objectReferenceValue as ItemType).name;
            }
            EditorGUI.LabelField(new Rect(rect.x, rect.y + 1, rect.width, EditorGUIUtility.singleLineHeight), name);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(m_DeathmatchAgent, "Inspector");
                serializedObject.ApplyModifiedProperties();
                InspectorUtility.SetObjectDirty(m_DeathmatchAgent);
            }
        }

        /// <summary>
        /// The ReordableList add button has been pressed. Add a new weapon.
        /// </summary>
        private void OnAvailableWeaponListAdd(ReorderableList list)
        {
            m_AvailableWeapons.InsertArrayElementAtIndex(m_AvailableWeapons.arraySize);
            list.index = m_DeathmatchAgent.SelectedAvailableWeapon = m_AvailableWeapons.arraySize - 1;
            m_AvailableWeapons.serializedObject.ApplyModifiedProperties();
            InspectorUtility.SetObjectDirty(m_DeathmatchAgent);
        }

        /// <summary>
        /// The ReordableList remove button has been pressed. Remove the selected weapon.
        /// </summary>
        private void OnAvailableWeaponListRemove(ReorderableList list)
        {
            m_AvailableWeapons.DeleteArrayElementAtIndex(list.index);
            list.index = list.index - 1;
            if (list.index == -1 && m_AvailableWeapons.arraySize > 0) {
                list.index = 0;
            }
            m_DeathmatchAgent.SelectedAvailableWeapon = list.index;
            InspectorUtility.SetObjectDirty(m_DeathmatchAgent);
        }

        /// <summary>
        /// Updates the items to the correct Deathmatch classes.
        /// </summary>
        public static void UpdateItemClasses(DeathmatchAgent deathmatchAgent)
        {
            var items = deathmatchAgent.GetComponentsInChildren<Item>();
            for (int i = items.Length - 1; i > -1; --i) {
                if (items[i].GetType().Equals(typeof(ShootableWeapon)) || items[i].GetType().Equals(typeof(ThirdPersonController.Wrappers.ShootableWeapon))) {
                    var deathmatchShootableWeapon = items[i].gameObject.AddComponent<DeathmatchShootableWeapon>();
                    EditorUtility.CopySerialized(items[i], deathmatchShootableWeapon);
                    DestroyImmediate(items[i], true);
                } else if(items[i].GetType().Equals(typeof(ThrowableItem)) || items[i].GetType().Equals(typeof(ThirdPersonController.Wrappers.ThrowableItem))) {
                    var deathmatchThrowableWeapon = items[i].gameObject.AddComponent<DeathmatchThrowableItem>();
                    EditorUtility.CopySerialized(items[i], deathmatchThrowableWeapon);
                    DestroyImmediate(items[i], true);
                }
            }
        }
    }
}