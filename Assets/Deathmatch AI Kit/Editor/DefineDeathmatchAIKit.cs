﻿using UnityEditor;

/// <summary>
/// Editor script which will define the Deathmatch AI Kit symbol so the Character Builder is aware that a deathmatch agent can be created.
/// </summary>
[InitializeOnLoad]
public class DefineMorph3D : Editor
{
    private static string s_DeathmatchAIKitSymbol = "DEATHMATCH_AI_KIT_PRESENT";

    /// <summary>
    /// Add the Deathmatch AI Kit symbol as soon as Unity gets done compiling.
    /// </summary>
    static DefineMorph3D()
    {
        var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
        if (!symbols.Contains(s_DeathmatchAIKitSymbol)) {
            symbols += (";" + s_DeathmatchAIKitSymbol);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, symbols);
        }
    }
}
